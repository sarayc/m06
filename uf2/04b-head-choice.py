#! /usr/bin/python3
#-*- coding: utf-8 -*-
# 
# @sarayc M06 Curs 2023-2024
# head [-n 5|10|15] file
# default=10, file o stdin
# ----------------------------------
import sys, argparse
parser = argparse.ArgumentParser(\
    description="Mostrar les N primeres linies",\
    epilog = "thats all folks")

parser.add_argument("-n","--nlin", type=int,\
    help="Numero de linies", \
    dest="nlin",\
    default=10,\
    metavar="numLines",\
    choices=[5,10,15])

parser.add_argument("fitxer", type=str,\
    help="fitxer a processar",\
    metavar="file")

args=parser.parse_args()
print(args)


# ---------------------------------
MAX=args.nlin
counter=0

fileIn=open(args.fitxer,"r")

for line in fileIn:
    counter+=1
    print(line,end="")
    
    if counter==MAX:
        break

fileIn.close()
exit(0)

