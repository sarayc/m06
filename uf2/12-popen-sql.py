#! /usr/bin/python3
#-*- coding: utf-8 -*-
# 
# @sarayc M06 Curs 2023-2024
# popen-sql.py
# --------------------------------------
import sys, argparse
from subprocess import Popen, PIPE
command = ["psql -qtA -F',' -h localhost -U postgres training -c \"select * from oficinas; \""]
pipeData = Popen(command, shell=True,stdout=PIPE)
for line in pipeData.stdout:
    print(line.decode("utf-8"),end="")
exit(0)
