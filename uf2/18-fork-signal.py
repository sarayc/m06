#! /usr/bin/python3
#-*- coding: utf-8 -*-
# 
# @sarayc M06 Curs 2023-2024
# exemple-fork.py
# --------------------------------------
import sys,os,signal

def usr1(signum,frame):
    print("Signal handler with signal: ", signum)
    print("hola")

def usr2(signum,frame):
    print("Signal handler with signal: ", signum)
    print("Adeu")
    sys.exit(0)

print("Hola començament del programa principal")
print("PID pare: ", os.getpid())
pid=os.fork()
if pid != 0:
   # os.wait()
    print("Programa pare: ", os.getpid(), pid)
    print("Llançant el procés fill servidor")
    sys.exit(0)

print("Programa fill: ", os.getpid(), pid)
# Assignar senyals
signal.signal(signal.SIGUSR1, usr1) # 10
signal.signal(signal.SIGUSR2, usr2) # 12
while True:
    pass


print("Hasta luego")
sys.exit(0)
