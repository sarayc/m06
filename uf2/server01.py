#! /usr/bin/python3
#-*- coding: utf-8 -*-
# @sarayc
# Examen python
# 10/05/2024
# ----------------------------
import sys, argparse, signal, socket, os
from subprocess import Popen,PIPE
parser = argparse.ArgumentParser(description ="programa")
parser.add_argument("-d","--debug", action="store_true", default=False)
parser.add_argument("-p","--port",type=int,metavar="port", dest="port",default=44444)
args = parser.parse_args()
#print(args)
llistaPeers = []

def myusr1(signum, frame):
    print("Signal handler called with signal: ", signum)
    print(llistaPeers)
    sys.exit(0)

def myusr2(signum, frame):
    print("Signal handler called with signal: ", signum)
    print(len(llistaPeers))
    sys.exit(0)

def myterm(signum, frame):
    print("Signal handler called with signal: ", signum)
    print(llistaPeers, len(llistaPeers))
    sys.exit(0)

signal.signal(signal.SIGUSR1, myusr1) # senyal 10
signal.signal(signal.SIGUSR2, myusr2) # senyal 12
signal.signal(signal.SIGTERM, myterm) # senyal 15

# ----------------------------
HOST = ''
PORT = args.port
DEBUG = args.debug
s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
s.bind((HOST,PORT))
s.listen(1)
ACABAR=bytes(chr(4),'utf-8')
#id = os.fork()
#if pid != 0:
#   print("Engegat el servei: ", pid)
#    sys.exit(0)
# per cada client
while True:
    conn, addr = s.accept()
    print("Connected by", addr)
    llistaPeers.append(addr)
    # per cada ordre del client
    while True:
        command = conn.recv(1024)
        # si la commanda és processos executa i envia ps ax
        if DEBUG: print("Recive", repr(command))
        print(command.decode('utf-8'))
        if not command: break
        if command.strip() == b'': break
        if command.strip() == b"processos":
            command = ["ps ax"]
        elif command.strip() == b"ports":
            command = ["ss -pta"]
        else:
            command = ["uname -a"]
        pipeData = Popen(command,shell=True,stdout=PIPE, stderr=PIPE)
        for line in pipeData.stdout:
            conn.sendall(line)
            if DEBUG: sys.stderr.write(str(line,'utf-8'))
        for line in pipeData.stderr:
            conn.sendall(line)
            if DEBUG: sys.stderr.write(str(line,'utf-8'))
        conn.sendall(ACABAR)
    conn.close()
s.close()
sys.exit(0)


