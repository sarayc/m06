#! /usr/bin/python3
#-*- coding: utf-8 -*-
# 
# @sarayc M06 Curs 2023-2024
# exemple-fork.py
# --------------------------------------
import sys,os
print("Hola començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid != 0:
   # os.wait()
    print("Programa pare: ", os.getpid(), pid)
    sys.exit(0)

# programa fill
#os.execv("/usr/bin/ls", ["/usr/bin/ls", "-ls","/"])
#os.execl("/usr/bin/ls","/usr/bin/ls","-la","/")
#os.execlp("ls","ls","-la","/")
#os.execvp("/usr/bin/uname",["uname","-a"])
#os.execv("/bin/bash", ["/bin/bash","show.sh"])
os.execle("/bin/bash","/bin/bash","show.sh",{"nom":"joan", "edat":"25"})
print("Hasta luego")
sys.exit(0)
