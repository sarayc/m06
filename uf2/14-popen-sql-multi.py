#! /usr/bin/python3
#-*- coding: utf-8 -*-
# 
# @sarayc M06 Curs 2023-2024
# popen-sql.py -c num_clie [-c numclie...]
# --------------------------------------
import sys, argparse
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description=\
        """Consulta a la base de dades entrada per argument al programa.""")
parser.add_argument("-c","--client",type=str,\
        help="consulta a la base de dades training", \
        action="append",\
        metavar="numclie",\
        dest="clieList")
args=parser.parse_args()
#print(args)
#exit(0)
# -------------------------------
command = "PGPASSWORD=passwd  psql -qtA -F',' -h localhost  -U postgres training"
pipeData = Popen(command, shell=True, bufsize=0, \
             universal_newlines=True,
        	 stdin=PIPE, stdout=PIPE, stderr=PIPE)

for num_clie in args.clieList:
    sqlStatement="select * from clientes where num_clie=%s;" % num_clie
    pipeData.stdin.write(sqlStatement+"\n")
    print(pipeData.stdout.readline(),end="")
pipeData.stdin.write("\q\n")
exit(0)

