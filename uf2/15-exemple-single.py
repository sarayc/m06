#! /usr/bin/python3
#-*- coding: utf-8 -*-
# 
# @sarayc M06 Curs 2023-2024
# singla-exemple.py
# --------------------------------------
import sys, os, signal
def myhandler(signum, framei):
    print("Signal handler with signal: ", signum)
    print("hasta luego lucas")
    sys.exit(0)

def nodeath(signum, frame):
    print("Signal handler with signal: ", signum)
    print("Tururu")

# Assignar un handler al senyal
signal.signal(signal.SIGUSR1,myhandler) #10
signal.signal(signal.SIGUSR2, nodeath) #12
signal.signal(signal.SIGALRM, nodeath) #14
signal.signal(signal.SIGTERM, signal.SIG_IGN) #15
signal.signal(signal.SIGTINT, signal.SIG_IGN) #2

signal.alarm(60)
print(os.getpid())
while True:
    pass
sys.exit(0)
