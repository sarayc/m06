#! /usr/bin/python3
#-*- coding: utf-8 -*-
# 
# @sarayc M06 Curs 2023-2024
# head [-n 5|10|15] -v file...
# default=10
# ----------------------------------
import sys, argparse
parser = argparse.ArgumentParser(\
    description="Mostrar les N primeres linies",\
    epilog = "thats all folks")

parser.add_argument("-n",type=int,\
    help="Numero de linies", \
    dest="nlin",\
    default=10,\
    metavar="numLines",\
    choices=[5,10,15])

parser.add_argument("-v", '--verbose', action='store_true')
parser.add_argument("fileList",type=str,\
    nargs="*" ,\
    help="fitxer a processar",\
    metavar="file")

args=parser.parse_args()
print(args)
#sys.exit(0)
# ---------------------------------
MAX=args.nlin

def headFile(file):
    counter=0
    fileIn=open(file,"r")
    for line in fileIn:
        counter+=1
        print(line,end="")
        if counter==MAX:
            break
    fileIn.close()
if args.fileList:
    for fileName in args.fileList:
        if  args.verbose:
            print("\n", fileName, 40*"-")
        headFile(fileName)

exit(0)
