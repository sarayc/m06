import sys, socket, argparse, signal
from subprocess import Popen, PIPE
import datetime
parser=argparse.ArgumentParser(description="telnet server")
parser.add_argument("-p","--port", type=int,\
        help="port del servei",\
        metavar="port",\
        dest="port",\
        default=50001)
parser.add_argument("-d", "--debug", \
        action="store_true",\
        help="accions")
args=parser.parse_args()
HOST=''
PORT=args.port
ACABAR=bytes(chr(4),'utf-8')
DEBUG=args.debug
llistaPeers=[]
# -------------------------------
def myusr1(signum, frame):
    print("Signal handler called with signal: ", signum)
    print(llistaPeers)
    sys.exit(0)

def myusr2(signum, frame):
    print("Signal handler called with signal: ", signum)
    print(len(llistaPeers))
    sys.exit(0)

def myterm(signum, frame):
    print("Signal handler called with signal: ", signum)
    print(llistaPeers, len(llistaPeers))
    sys.exit(0)

signal.signal(signal.SIGUSR1, myusr1) # senyal 10
signal.signal(signal.SIGUSR2, myusr2) # senyal 12
signal.signal(signal.SIGTERM, myterm) # senyal 15

# ------------------------------
s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
s.bind((HOST,PORT))
s.listen(1)
#pid = os.fork()
#if pid != 0:
#    print("Programa pare acabar")
#    sys.exit(0)
#print("Programa principal: ", os.getpid(), pid)

while True:
    conn, addr = s.accept()
    print("Connexió per: ", addr)
    llistaPeers.append((addr, datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")))
    while True:
        ordre = conn.recv(1024)
        if DEBUG: print("Recive", repr(ordre))
        if not ordre: break
        pipeData = Popen(ordre, stdout=PIPE, stderr=PIPE, shell=True)
        for line in pipeData.stdout:
            conn.sendall(line)
            if DEBUG: sys.stderr.write(str(line,'utf-8'))
        for line in pipeData.stderr:
            conn.sendall(line)
            if DEBUG: sys.stderr.write(str(line,'utf-8'))
        conn.sendall(ACABAR)
    conn.close()
s.close()
sys.exit(0)

