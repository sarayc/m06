#! /usr/bin/python3
#-*- coding: utf-8 -*-
# 
# @sarayc M06 Curs 2023-2024
# telnet-client.py
# --------------------------------------
# Importem els mòduls
import sys,socket,argparse,os
from subprocess import Popen, PIPE
# Arguments
parser = argparse.ArgumentParser(description="telnet client")
parser.add_argument("-p", "--port",type=int, \
        dest="port",\
        metavar="port",\
        required=True)
parser.add_argument("-s","--server", type=str,\
        dest="server",\
        metavar="server",\
        required=True)
args=parser.parse_args()
# Assignem les variables
HOST = args.server
PORT = args.port
ACABAR = bytes(chr(4),'utf-8')
# -------------------------------------------------
# Creem un socket TCP
s=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
s.connect((HOST,PORT))
# bucle
# L'usuari realitza ordre i l'envia al servidor
# el servidor envia el resultat i el client la mostra per pantalla
while True:
    # l'usuari realitza una comanda
#    ordre = input(f"{HOST}:{PORT}> ")
    ordre=input("$")
    # verifiquem si no hem ingresat cap ordre
    if not ordre: break
    # sendall per garantir que totes les dades s'envien
    s.sendall(bytes(ordre,'utf-8'))
    # bucle per rebre la resposta del servidor
    while True:
        data = s.recv(1024)
        if data[-1:] == ACABAR:
            print(str(data[:-1]))
            break
        print(str(data))
s.close()
sys.exit(0)
