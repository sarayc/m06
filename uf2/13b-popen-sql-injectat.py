#! /usr/bin/python3
#-*- coding: utf-8 -*-
# 
# @sarayc M06 Curs 2023-2024
# popen-sql.py
# --------------------------------------
import sys, argparse
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description=\
        """Consulta a la base de dades entrada per argument al programa.""")
parser.add_argument("sqlStatement",type=str,\
        help="consulta a la base de dades training")
args=parser.parse_args()
# -------------------------------
command = "PGPASSWORD=passwd  psql -qtA -F',' -h localhost  -U postgres training"
pipeData = Popen(command, shell=True, bufsize=0, \
             universal_newlines=True,
        	 stdin=PIPE, stdout=PIPE, stderr=PIPE)
pipeData.stdin.write(args.sqlStatement+"\n\q\n")
for line in pipeData.stdout:
    print(line,end="")
exit(0)

