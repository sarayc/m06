#! /usr/bin/python3
#-*- coding: utf-8 -*-
# 
# @sarayc M06 Curs 2023-2024
# execv-signal.py
# --------------------------------------
import sys,os
print("Hola començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid != 0:
   # os.wait()
    print("Programa pare: ", os.getpid(), pid)
    sys.exit(0)

# programa fill
os.execv("/usr/bin/python3", ["/usr/bin/python3", "16-signal.py","70"])
print("Hasta luego")
sys.exit(0)
