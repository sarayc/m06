#!/usr/bin/python 
#-*- coding: utf-8-*-
# carrega el mòdul 
import pam
p = pam.pam()

nom_usuari = input("Introdueix el nom de l'usuari: ")
contrasenya = input("Introdueix el password: ")

p.authenticate(nom_usuari, contrasenya)

if p.code == 0:
	for i in range(1,11):
		print(i)
else:
	print("Usuari no autenticat")
