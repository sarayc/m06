## PAM LDAP
### Per accedir
**1) docker-compose.yml**
```
version: "2:"
services:
  ldap:
    image: sarayj03/ldap23:latest
    container_name: ldap.edt.org
    hostname: ldap.edt.org
    ports:
      - "389:389"
    networks:
      - 2hisx
  pam:
    image: sarayj03/pam23:ldap
    privileged: true
    container_name: pam.edt.org
    hostname: pam.edt.org
    networks:
      - 2hisx
networks:
  2hisx:
```

**2)Entrar manualment**
```
docker run --rm --name pam.edt.org -h pam.edt.org --privileged --net 2hisx -d sarayj03/pam23:ldap
```

```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -d sarayj03/ldap23:latest
```

### Fitxers 
Fitxers que necessitem
+ Dockerfile 
Afegim els paquets que necessitem
	+ libnss-ldapd: permet a les aplicacions buscar usuaris, grups i hosts,etc, utilitzant un directori ldap per mitjans de la interfície NSS (que permet a les aplicacions autenticar-se utilitzant LDAP). Es configura amb /etc/nss-ldapd.conf, també /etc/nsswithc.conf.

	+ libpam-ldapd: permet a les aplicacions utilitzar PAM per autenticar-se, mitjaçant un servidor LDAP. L’arxiu de configuració està a /etc/nslcd.

	+ nslcd: dimoni que es pot utilitza per integrar sistemes linux amb els serveis LDAP.

	+ nslcd-utils: proporciona eines per consultar i actualitzar informació a LDAP mitjançant nslcd.

```
# pam
FROM debian:11
LABEL version="1.0"
LABEL author="@edt ASIX-M06"
LABEL subject="PAM host"
RUN apt-get update
#ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -y install procps iproute2 tree nmap vim less finger passwd  libnss-ldapd libpam-ldapd nslcd nslcd-utils ldap-utils libpam-mount libpam-pwquality
RUN mkdir /opt/docker
COPY ldap.conf /etc/ldap/ldap.conf
COPY nslcd.conf /etc/nslcd.conf
COPY nsswitch.conf /etc/nsswitch.conf
COPY pam_mount.conf.xml /etc/security/pam_mount.conf.xml
COPY common-session /etc/pam.d/common-session
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
```


+ startup.sh
```
#! /bin/bash

for user in unix01 unix02 unix03 unix04 unix05
do
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | passwd $user
done


#cp /opt/docker/nslcd.conf /etc/nslcd.conf
#cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
#cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
#cp /opt/docker/common-account /etc/pam.d/common-account
#cp /opt/docker/common-auth /etc/pam.d/common-auth
#cp /opt/docker/common-password /etc/pam.d/common-password
#cp /opt/docker/common-session /etc/pam.d/common-session
#cp /opt/docker/pam_mount.conf.xml /etc/security/pam_mount.conf.xml

/usr/sbin/nscd
/usr/sbin/nslcd
sleep infinity
/bin/bash
```


+ ldap.conf
Fitxer que està a /etc/ldap/ i ens serveix per saber amb qui a de contactar.
```
#
# LDAP Defaults
#

# See ldap.conf(5) for details
# This file should be world readable but not world writable.

#BASE	dc=example,dc=com
#URI	ldap://ldap.example.com ldap://ldap-provider.example.com:666

#SIZELIMIT	12
#TIMELIMIT	15
#DEREF		never

# TLS certificates (needed for GnuTLS)
TLS_CACERT	/etc/ssl/certs/ca-certificates.crt

BASE dc=edt,dc=org
URI ldap://ldap.edt.org
```


+ nslcd.conf
Està a /etc/nslcd.conf, on posem la uri i la base de dades
```
# /etc/nslcd.conf
# nslcd configuration file. See nslcd.conf(5)
# for details.

# The user and group nslcd should run as.
uid nslcd
gid nslcd

# The location at which the LDAP server(s) should be reachable.
uri ldap://ldap.edt.org
#uri ldap://172.18.0.3

# The search base that will be used for all queries.
base dc=edt,dc=org

# The LDAP protocol version to use.
#ldap_version 3

# The DN to bind with for normal lookups.
#binddn cn=annonymous,dc=example,dc=net
#bindpw secret

# The DN used for password modifications by root.
#rootpwmoddn cn=admin,dc=example,dc=com

# SSL options
#ssl off
#tls_reqcert never
tls_cacertfile /etc/ssl/certs/ca-certificates.crt

# The search scope.
#scope sub
``` 


+ nsswitch.conf
Fitxer que està a /etc/ ens serveix per saber a on anirà a buscar i la propritat que té. Primer anirà a buscar localment i si no o troba, anirà a ldap.
```
# /etc/nsswitch.conf
#
# Example configuration of GNU Name Service Switch functionality.
# If you have the `glibc-doc-reference' and `info' packages installed, try:
# `info libc "Name Service Switch"' for information about this file.

passwd:         files ldap
group:          files ldap
shadow:         files ldap
gshadow:        files ldap

hosts:          files dns
networks:       files

protocols:      db files
services:       db files
ethers:         db files
rpc:            db files

netgroup:       nis
```


+ pam_mount.conf.xml
Aquest fitxer està a /etc/security/. On montarem el volum.
```
	<volume
			user="*"
			fstype="tmpfs"
			options="size=100M"
			mountpoint="~/mytmp"
		/>
```


+ common-session
Aquest fitxer es troba a /etc/pam.d/. Afegim mkhomedir perquè ens crei el home si no el té creat.
```
session	required	pam_unix.so 
session optional 	pam_mkhomedir.so
```



### Comprovacions:
1) Comprovació amb getent passwd i group
```
root@pam:/opt/docker# getent passwd jordi
jordi:*:5004:100:Jordi Mas:/tmp/home/jordi:
```

```
root@pam:/opt/docker# getent group 1wiam
1wiam:*:612:
```


2) Accedim als usuaris
**Usuari jordi**
Com és la primera vegada que entrem, s'ha creat el directori home perquè no el tenim, en canvi, si entremcom a usuari unix01 no es crea perquè ja està creat.
```
root@pam:/opt/docker# su -l unix01
reenter password for pam_mount:
unix01@pam:~$ su -l jordi
Password: 
Creating directory '/tmp/home/jordi'.
```


**Usuari unix02**
```
unix01@pam:~$ su -l unix02
Password: 
unix02@pam:~$ 
```


3) Canvi de contrasenya 

**Usuari unix01**
```
root@d2762930b9f7:/opt/docker# su -l unix01
reenter password for pam_mount:
unix01@d2762930b9f7:~$ passwd 	 
Changing password for unix01.
Current password:
New password:
Retype new password:
passwd: password updated successfully
```


**Usuari jordi**
```
unix02@pam:~$ su -l jordi
Password: 
$ passwd 
(current) LDAP Password: 
New password: 
Retype new password: 
passwd: password updated successfully
```


4) Volum

```
unix01@b73303771781:~$ ls
mytmp
```
 




