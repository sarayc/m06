<<<<<<< HEAD
## Servidor PAM
### Fitxers:
+ Dockerfile
+ startup.sh

Entrem de manera interactiva 
```
docker run --rm --name pam.edt.org -h pam.edt.org --net 2hisx -it sarayj03/pam23:base 
```


#### Fitxer Dockerfile
```
# pam
FROM debian:latest
LABEL version="1.0"
LABEL author="@edt ASIX-M06"
LABEL subject="PAM host"
RUN apt-get update
#ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -y install procps iproute2 tree nmap vim less finger passwd libpam-pwquality libpam-mount 
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
```

+ Fitxer startup.sh
```
#! /bin/bash

for user in unix01 unix02 unix03 unix04 unix05
do
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | passwd $user
done


/bin/bash
```

Com tenim les contrasenyes de manera que és siguin igual que el password ens surt aquest missatge quan iniciem el container
```
a211464sc@i07:~/m06/pam23/pam23:base$ docker run --rm --name pam.edt.org -h pam.edt.org --net 2hisx -it sarayj03/pam23:base
New password: BAD PASSWORD: The password is shorter than 8 characters
Retype new password: passwd: password updated successfully
New password: BAD PASSWORD: The password is shorter than 8 characters
Retype new password: passwd: password updated successfully
New password: BAD PASSWORD: The password is shorter than 8 characters
Retype new password: passwd: password updated successfully
New password: BAD PASSWORD: The password is shorter than 8 characters
Retype new password: passwd: password updated successfully
New password: BAD PASSWORD: The password is shorter than 8 characters
Retype new password: passwd: password updated successfully
root@pam:/opt/docker#   
```

**important** --> per comprovar el funcionament de les modificaciones del servei chfn cal executar l'ordre chfn des d'un usuari no root. Sembla que la pròpia ordre chfn identifica si l'usuari és el UID 0 i actua diferent que per a un usuari no privilegiat.

```
a211464sc@i07:~/m06/pam23/pam23:base$ docker exec -it pam.edt.org /bin/bash
root@pam:/opt/docker# su -l unix01
unix01@pam:~$ 
```

### Exemples
#### Exemple 01. L'usuari pot modificar sempre el seu chfn sense password
Usualment un usuari no privilegiat quan modifica el seu finger amf chfn és requerit (al final i si ha fet modificacions) d’identificar-se amb el seu password. Si l’usuari és root la pròpia ordre s’estalvia aquest pas i permet el canvi automàticament.

La següent configuració actualitza el chfn automàticament sense demanar password.

**Fitxer**
```
# proves 2hisx
auth optional pam_echo.so [dijous: %h %s %t %u ]
auth sufficient pam_permit.so
account optional pam_echo.so [account: %h %s %t %u]
```

**comprovació**
```
unix01@pam:~$ chfn 
dijous: pam.edt.org chfn (null) unix01 
account: pam.edt.org chfn (null) unix01
Changing the user information for unix01
Enter the new value, or press ENTER for the default
	Full Name: 
	Room Number []: 11
	Work Phone []: 11
	Home Phone []: 11
unix01@pam:~$ finger unix01
Login: unix01         			Name: 
Directory: /home/unix01             	Shell: /bin/bash
Office: 11, 11				Home Phone: 11
Never logged in.
No mail.
No Plan.
```

=======
## PAM BASE


>>>>>>> 94118f6bcf8d4b8772e55e1c92627030ffaeef6f
