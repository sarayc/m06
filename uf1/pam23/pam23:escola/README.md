## PAM Escola
Configurar el container PAM (--privileged) per:
+ permetre l'autenticació local dels usuaris unix locals (unix01, unix02 i unix03).
+ permetre l'autenticació d'usuaris de LDAP de l'escola del treball.
+ als usuaris LDAP se'ls ha de crear el directori home automàticament si no existeix.
+ als usuaris se'ls munta automàticament dins el seu home un directori anomenat cloud corresponent al recurs WebDav de l'unitat de xarxa de l'escola de l'usuari.

Observacions de configuració:
+ URI ldap.escoladeltreball.org
+ BASE dc=escoladeltreball,dc=org
+ WevDav https://cloud.proxy.inf.edt.cat:5511/remote.php/dav/files/<usuari>


### Per accedir
**1) docker-compose.yml**
```
version: "2:"
services:
  ldap:
    image: sarayj03/ldap23:latest
    container_name: ldap.edt.org
    hostname: ldap.edt.org
    ports:
      - "389:389"
    networks:
      - 2hisx
  pam:
    image: sarayj03/pam23:ldap
    privileged: true
    container_name: pam.edt.org
    hostname: pam.edt.org
    networks:
      - 2hisx
networks:
  2hisx:
```

**2)Entrar manualment**
```
docker run --rm --name pam.edt.org -h pam.edt.org --privileged -d sarayj03/pam23:escola
```

### Fitxers
+ Dockerfile
+ startup.sh
+ nsswitch.conf 
+ nslcd.conf
+ common-session
+ ldap.conf
```
BASE dc=escoladeltreball,dc=org
URI ldap://ldap.escoladeltreball.org
```




### Passos
#### Intentem contactar contra l'escola del treball
Primer fem **nmap  ldap.escoladeltreball.org** 
Si fem **ldapsearch -x** no contacta amb ningú, per tant, editem el fitxer client.
```
BASE dc=escoladeltreball,dc=org
URI ldap://ldap.escoladeltreball.org
```

```
root@pam:/opt/docker# ldapsearch -x | head
# extended LDIF
#
# LDAPv3
# base <dc=escoladeltreball,dc=org> (default) with scope subtree
# filter: (objectclass=*)
# requesting: ALL
#

# escoladeltreball.org
dn: dc=escoladeltreball,dc=org
```


#### Editem el fitxer nslcd.conf
```
uri ldap://ldap.escoladeltreball.org
#uri ldap://172.18.0.3

# The search base that will be used for all queries.
base dc=escoladeltreball,dc=org
```


#### Editem el fitxer nsswitch.conf
```
passwd:         files ldap
group:          files ldap
```


### Comprovacions:
#### 1) contactem amb escoladeltreball.org
```
root@pam:/opt/docker# ldapsearch -x | head
# extended LDIF
#
# LDAPv3
# base <dc=escoladeltreball,dc=org> (default) with scope subtree
# filter: (objectclass=*)
# requesting: ALL
#

# escoladeltreball.org
dn: dc=escoladeltreball,dc=org
```

#### 2) Permetre autenticació local dels usuaris locals
Ens dona un error al mount però ja és correcte.
```
root@pam:/opt/docker# su -l unix01
reenter password for pam_mount:
(mount.c:68): Messages from underlying mount program:
(mount.c:72): /sbin/mount.davfs: Mounting failed.
(mount.c:72): Could not authenticate to server: rejected Basic challenge
(pam_mount.c:522): mount of https://cloud.proxy.inf.edt.cat:5511/remote.php/dav/files/unix01 failed
unix01@pam:~$
```


#### 3) Permetre l'autenticació d'usuaris de LDAP de l'escola del treball
#### 4) Se'ls ha de creat automàticament el directori home
```
root@pam:/opt/docker# su -l a211464sc

Creating directory '/home/users/inf/hisx2/a211464sc'.
```


#### 5) Als usuaris se'ls munta automàticament dins el seu home un directori anomenat cloud
```
a211464sc@i07:~/m06/pam23$ docker exec -it pam.edt.org /bin/bash
root@pam:/opt/docker# su -l a211464sc

Creating directory '/home/users/inf/hisx2/a211464sc'.
reenter password for pam_mount:
(mount.c:68): Messages from underlying mount program:
(mount.c:72): /sbin/mount.davfs: warning: the server does not support locks

a211464sc@pam:~$ ls
cloud  mytmp
a211464sc@pam:~$ 
```
