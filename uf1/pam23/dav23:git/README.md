## DAV git
Aquesta imatge serà un servidor WebDav que compartirà per HTTP i WebDav els continguts que es publiquin al directori /var/www/webdav. Posar en aquest directori per exemple els apunts del gitlab edtasixm06 corresponents a m06-aso.

## Pràctica (a)
### Fitxers
+ Dockerfile
```
FROM debian:11
LABEL version="1.0"
LABEL subject="PAM"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -y install procps iproute2 tree nmap vim less finger passwd git apache2 davfs2
RUN a2enmod dav_fs auth_digest
COPY apache2.conf /etc/apache2/apache2.conf
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
```


+ startup.sh
```
#! /bin/bash

for user in unix01 unix02 unix03 unix04 unix05
do
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | passwd $user
done


mkdir /var/www/webdav
cd /var/www/webdav
git clone https://gitlab.com/sarayc/m06.git
cd /opt/docker

a2enmod dav_fs
a2enmod auth_digest

#cp apache2-webdav.conf /etc/apache2/conf-enabled
apachectl -DFOREGROUND -k start
```


+ apache2.conf
```
Alias /webdav /var/www/webdav
<Location /webdav>
	Dav on
	Require all granted
</Location>
```


## Pràctica (b)
Creem un docker-compose.yml
```
version: "2:"
services:
  ldap:
    image: sarayj03/ldap23:latest
    container_name: ldap.edt.org
    hostname: ldap.edt.org
    ports:
      - "389:389"
    networks:
      - 2hisx
  pam:
    image: sarayj03/pam23:ldap
    privileged: true
    container_name: pam.edt.org
    hostname: pam.edt.org
    networks:
      - 2hisx
  dav:
    image: sarayj03/dav23:git
    container_name: dav.edt.org
    hostname: dav.edt.org
    privileged: true
    ports:
      - "80:80"
    volumes:
      - "data-dav:/var/www/webdav"
    networks:
      - 2hisx
networks:
  2hisx:
volumes:
  data-dav:
```


### Comprovacions
#### 1) Posem entrar amb un usuari unix01
```
root@pam:/opt/docker# su -l unix01
reenter password for pam_mount:
unix01@pam:~$ ls
tmp
```


#### 2) Posem entrar amb un usuari ldap i és crea el directori home si no està creat
```
unix01@pam:~$ su -l pere
Password: 
Creating directory '/tmp/home/pere'.
```


#### 3) Es munta un recurs apunts dins del home
```
unix01@pam:~$ su -l pere
Password: 
Creating directory '/tmp/home/pere'.
$ ls
apunts	tmp
$
```



