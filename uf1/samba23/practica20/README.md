## Pràctica 20

Tenim un docker compose per tenir-ho tot.
+ Container pam farà de client
+ container samba servidor
+ container ldap servidor

```
version: "2"
services:
  ldap:
	image: sarayj03/ldap23:latest
	container_name: ldap.edt.org
	hostname: ldap.edt.org
	ports:
  	- "389:389"
	networks:
  	- 2hisx
  pam:
	image: sarayj03/pam23:base
	privileged: true
	container_name: pam.edt.org
	hostname: pam.edt.org
	networks:
  	- 2hisx
  samba:
	image: sarayj03/samba23:base
	privileged: true
	container_name: samba.edt.org
	hostname: samba.edt.org
	networks:
  	- 2hisx
networks:
  2hisx:
```


Comprovem la conectivitat amb un nmap
```
root@client:/opt/docker# nmap samba.edt.org
Starting Nmap 7.93 ( https://nmap.org ) at 2024-03-17 17:08 UTC
Nmap scan report for samba.edt.org (172.19.0.2)
Host is up (0.0000080s latency).
rDNS record for 172.19.0.2: samba.edt.org.2hisx
Not shown: 998 closed tcp ports (reset)
PORT	STATE SERVICE
139/tcp open  netbios-ssn
445/tcp open  microsoft-ds
MAC Address: 02:42:AC:13:00:02 (Unknown)

Nmap done: 1 IP address (1 host up) scanned in 0.20 seconds
```


Llistem els recursos que tenim
```
root@client:/opt/docker# smbclient -L //samba.edt.org
Password for [MYGROUP\root]:
Anonymous login successful

    Sharename   	Type  	Comment
    ---------   	----  	-------
    documentation   Disk  	Documentació doc del container
    manpages    	Disk  	Documentació man del container
    public      	Disk  	Share de contingut public
    IPC$        	IPC   	IPC Service (Samba Server Version 4.17.12-Debian)
    nobody      	Disk  	Home Directories
SMB1 disabled -- no workgroup available
```


Llistem els usuaris samba
```
lila:1000:
patipla:1002:
roc:1001:
pla:1003:
```


### Exemples HOW TO ASIX
#### Exemple 1
**usuari guest fa idmap a nobody**
Observem que l’accés al dir de l’usuari anònim guest es transforma (id mapping) en l’usuari unix nobody. Tenim la següent configuració amb fitxer smb.conf.

```
[public]
    	comment = Share de contingut public
    	path = /var/lib/samba/public
    	public = yes
    	browseable = yes
    	writable = yes
    	printable = no
    	guest ok = yes → tenir això és el mateix que tenim public=yes
```

Anem al container client i entrem al recurs public, després realitzem un put per pujar per exemple el fitxer /etc/os-release amb el nom file1.

```
root@client:/opt/docker# smbclient //samba.edt.org/public
Password for [MYGROUP\root]:
Anonymous login successful
Try "help" to get a list of possible commands.

smb: \> put /etc/os-release file1
putting file /etc/os-release as \file1 (43.5 kb/s) (average 43.5 kb/s)
smb: \> exit
```

Observem que s’ha creat amb l’usuari nobody perquè no ho podem associar a un usuari específic.
```
root@samba:/opt/docker# ls -la /var/lib/samba/public/
total 28
drwxrwxrwx 2 root   root	4096 Mar 17 17:29 .
drwxr-xr-x 1 root   root	4096 Mar 17 17:07 ..
-rw-r--r-- 1 root   root 	389 Mar 17 17:07 Dockerfile
-rwxr--r-- 1 nobody nogroup  267 Mar 17 17:29 file1
-rw-r--r-- 1 root   root	1510 Mar 17 17:07 smb.conf
-rwxr-xr-x 1 root   root 	612 Mar 17 17:07 startup.sh
```


#### Exemple 2
**només usuari guest**
Permet únicament accedir al recurs via usuari anonònim. No es permet l’accés via usuari identificat. Observem que el sistema ens engaya, ens diu que ens deixa entrar com l’usuari lila però en realitat som nobody.

```
[public]
    	comment = Share de contingut public
    	path = /var/lib/samba/public
    	browseable = yes
    	writable = yes
    	printable = no
    	guest only = yes
```

```
root@client:/opt/docker# smbclient -U lila //samba.edt.org/public
Password for [MYGROUP\lila]:
Try "help" to get a list of possible commands.

smb: \> get Dockerfile
getting file \Dockerfile of size 389 as Dockerfile (47.5 KiloBytes/sec) (average 47.5 KiloBytes/sec)

smb: \> put Dockerfile treball.txt
putting file Dockerfile as \treball.txt (126.6 kb/s) (average 126.6 kb/s)
smb: \>
```

```
root@samba:/opt/docker# ls -la /var/lib/samba/public/
total 32
drwxrwxrwx 2 root   root	4096 Mar 18 08:54 .
drwxr-xr-x 1 root   root	4096 Mar 18 08:52 ..
-rw-r--r-- 1 root   root 	389 Mar 18 08:52 Dockerfile
-rw-r--r-- 1 root   root	2764 Mar 18 08:52 REAMDE.md
-rw-r--r-- 1 root   root	1444 Mar 18 08:52 smb.conf
-rwxr-xr-x 1 root   root 	508 Mar 18 08:52 startup.sh
-rwxr--r-- 1 nobody nogroup  389 Mar 18 08:54 treball.txt
```


#### Exemple 3
**Usuari només guest amb idmap a un compte unix**
```
[public]
    	comment = Share de contingut public
    	path = /var/lib/samba/public
    	browseable = yes
    	writable = yes
    	printable = no
    	guest ok = yes
        guest account = pla
```

```
root@client:/opt/docker# smbclient //samba.edt.org/public
Password for [MYGROUP\root]:
Anonymous login successful
Try "help" to get a list of possible commands.

smb: \> put /etc/os-release file1
putting file /etc/os-release as \file1 (43.5 kb/s) (average 43.5 kb/s)
smb: \> exit
```

```
root@samba:/opt/docker# ls -la /var/lib/samba/public/
total 32
drwxrwxrwx 2 root   root	4096 Mar 17 17:46 .
drwxr-xr-x 1 root   root	4096 Mar 17 17:07 ..
-rw-r--r-- 1 root   root 	389 Mar 17 17:07 Dockerfile
-rwxr--r-- 1 nobody nogroup  267 Mar 17 17:29 file1
-rw-r--r-- 1 root   root	1510 Mar 17 17:07 smb.conf
-rwxr-xr-x 1 root   root 	612 Mar 17 17:07 startup.sh
-rwxr--r-- 1 lila   lila 	389 Mar 17 17:46 treball.txt
```


#### Exemple 4
**Usuari identificat**
Les ordres client samba permeten indicar en nom de quin usuari es vol realitzar la connexió. El password es pot demanar interactivament o indicar-lo en la línia de comanda. Així per exemple amb la ordres smbclient podem fer:
+ smbclient -U user //server//recurs
+ smbclient -U user%user //server/recurs


No posem la contrasenya a l’ordre i per tant ens la demana interactivament. 
```
root@client:/opt/docker# smbclient -U lila //samba.edt.org/public
Password for [MYGROUP\lila]:
Try "help" to get a list of possible commands.
smb: \>
```


En cas de posar una contrasenya errònia no ens deixarà entrar.
```
root@client:/opt/docker# smbclient -U lila //samba.edt.org/public
Password for [MYGROUP\lila]:
session setup failed: NT_STATUS_LOGON_FAILURE
```


Posem la contrasenya a la ordre.
```
root@client:/opt/docker# smbclient -U lila%lila //samba.edt.org/public
Try "help" to get a list of possible commands.
smb: \>
```


Fem una comprovació, eliminem el fitxer Dockerfile que tenim al container client, i ens ho descarreguem, també pujarem aquest fitxer amb el nom de treball.txt.
El fitxer Dockerfile que ens hem descarregat té com a propietari root, i el fitxer que hem pujat té com a propietari lila, que és qui ho ha pujat.
```
root@client:/opt/docker# rm Dockerfile
root@client:/opt/docker# smbclient -U lila%lila //samba.edt.org/public
Try "help" to get a list of possible commands.
smb: \> ls
  .                               	D    	0  Sun Mar 17 17:55:04 2024
  ..                              	D    	0  Sun Mar 17 17:07:43 2024
  startup.sh                      	N  	612  Sun Mar 17 17:07:43 2024
  smb.conf                        	N 	1510  Sun Mar 17 17:07:43 2024
  Dockerfile                      	N  	389  Sun Mar 17 17:07:43 2024

   	 33330872 blocks of size 1024. 6156616 blocks available

smb: \> get Dockerfile
getting file \Dockerfile of size 389 as Dockerfile (126.6 KiloBytes/sec) (average 126.6 KiloBytes/sec)

smb: \> put Dockerfile treball.txt
putting file Dockerfile as \treball.txt (95.0 kb/s) (average 95.0 kb/s)
smb: \> exit

root@client:/opt/docker# ls -la
total 40
drwxr-xr-x 1 root root  4096 Mar 17 18:01 .
drwxr-xr-x 1 root root  4096 Mar  9 10:03 ..
-rw------- 1 root root 12288 Mar  9 10:02 .REAMDE.md.swp
-rw-r--r-- 1 root root   389 Mar 17 18:01 Dockerfile
-rw-r--r-- 1 root root  1510 Mar  9 09:54 smb.conf
-rwxr-xr-x 1 root root   612 Mar  9 09:54 startup.sh
```

```
root@samba:/opt/docker# ls -la /var/lib/samba/public/
total 28
drwxrwxrwx 2 root root 4096 Mar 17 18:02 .
drwxr-xr-x 1 root root 4096 Mar 17 17:07 ..
-rw-r--r-- 1 root root  389 Mar 17 17:07 Dockerfile
-rw-r--r-- 1 root root 1510 Mar 17 17:07 smb.conf
-rwxr-xr-x 1 root root  612 Mar 17 17:07 startup.sh
-rwxr--r-- 1 lila lila  389 Mar 17 18:02 treball.txt
```


#### Exemple 5
**Valid users**
valid users = user1 user2 userN
Permet indicar la llista d’usuaris vàlids per accedir al recurs. La resta d’usuaris no podran accedir-hi. Tampoc guets tot i que s’hagi indicat guest ok.

```
[public]
    	comment = Share de contingut public
    	path = /var/lib/samba/public
    	browseable = yes
    	writable = yes
    	printable = no
    	guest ok = yes
        valid users= patipla roc
```

No tenim accés perquè no som ni patipla ni roc, però com a aquest usuaris si ens deixa entrar.

```
root@client:/opt/docker# smbclient //samba/public
Password for [WORKGROUP\root]:
tree connect failed: NT_STATUS_ACCESS_DENIED

root@client:/opt/docker# smbclient -U lila //samba/public
Password for [WORKGROUP\lila]:
tree connect failed: NT_STATUS_ACCESS_DENIED

root@client:/opt/docker# smbclient -U roc //samba/public
Password for [WORKGROUP\roc]:
Try "help" to get a list of possible commands.
smb: \>
```


#### Exemple 6
**Invalid users**
invalid users = user1 user2 userN
Indica la llista d’usuaris que no tenen permès accedir al recurs. La resta d’usuaris vàlids si hi poden accedir (guest dependrà de si s’ha permès o no via guest ok).

```
[public]
    	comment = Share de contingut public
    	path = /var/lib/samba/public
    	browseable = yes
    	writable = yes
    	printable = no
    	invalid users = patipla roc
    	guest ok = yes
```

Com a usuari lila podem entrar, però com a roc i patitpla no tenim accés.

```
root@client:/opt/docker# smbclient -U lila //samba/public
Password for [WORKGROUP\lila]:
Try "help" to get a list of possible commands.
smb: \> exi
root@client:/opt/docker# smbclient -U roc //samba/public
Password for [WORKGROUP\roc]:
tree connect failed: NT_STATUS_ACCESS_DENIED
```


#### Exemple 7
**Admin users**
admin users = user1 user2 userN
Permet definir un conjunt d’usuaris samba que seran convertits (id mapping) a l’usuari root. És a dir, estem dient que tal i tal usuari samba quan accedeixi al disc al recurs ha d’actuar com a usuari administrador (root en cas de unix).

```
[public]
   	 comment = Share de contingut public
   	 path = /var/lib/samba/public
   	 browseable = yes
   	 writable = yes
   	 printable = no
         guest ok = yes
         admin users = roc
```

```
root@client:/opt/docker# smbclient -U roc%roc //samba/public
Try "help" to get a list of possible commands.

smb: \> get Dockerfile
getting file \Dockerfile of size 389 as Dockerfile (379.8 KiloBytes/sec) (average 379.9 KiloBytes/sec)

smb: \> put Dockerfile roc.txt
putting file Dockerfile as \roc.txt (379.8 kb/s) (average 379.9 kb/s)
```


Comprovem que el fitxer roc.txt té com a propietari root.
```
root@samba:/opt/docker# ls -la /var/lib/samba/public/
total 44
drwxrwxrwx 2 root   root	4096 Mar 18 15:27 .
drwxr-xr-x 1 root   root	4096 Mar 18 15:07 ..
-rw-r--r-- 1 root   root 	389 Mar 18 15:07 Dockerfile
-rw-r--r-- 1 root   root	2764 Mar 18 15:07 REAMDE.md
-rwxr--r-- 1 root   roc  	389 Mar 18 15:27 roc.txt
-rw-r--r-- 1 root   root	8842 Mar 18 15:07 smb.conf
-rwxr-xr-x 1 root   root 	388 Mar 18 15:07 startup.sh
-rwxr--r-- 1 nobody nogroup  389 Mar 18 15:12 treball.txt
```


#### Exemple 8
**Recurs de només lectura**
read only = yes
writable = no
Els recursos es poden configurar per ser de només lectura (són equivalents).


```
[public]
   	 comment = Share de contingut public
   	 path = /var/lib/samba/public
  	 browseable = yes
         read only = yes
   	 writable = no
         guest ok = yes
```

Podem entrar però no podem pujar coses perquè no tenim permisos.
```
root@client:/opt/docker# smbclient -U lila //samba/public
Password for [WORKGROUP\lila]:
Try "help" to get a list of possible commands.

smb: \> get startup.sh
getting file \startup.sh of size 388 as startup.sh (378.9 KiloBytes/sec) (average 378.9 KiloBytes/sec)

smb: \> put startup.sh lila.txt
NT_STATUS_ACCESS_DENIED opening remote file \lila.txt
smb: \> exit

root@client:/opt/docker# smbclient //samba/public
Password for [WORKGROUP\root]:
Try "help" to get a list of possible commands.

smb: \> get startup.sh
getting file \startup.sh of size 388 as startup.sh (378.9 KiloBytes/sec) (average 378.9 KiloBytes/sec)

smb: \> put startup.sh nobody.txt
NT_STATUS_ACCESS_DENIED opening remote file \nobody.txt
smb: \>
```


#### Exemple 9
**Recurs de lectura / escriptura**
read only = no
writable = yes
Recursos configurats de lectura / escriptura. Són equivalents.


#### Exemple 10
read list = user1 user2 userN
Indica la llista d’usuaris que només poden llegir. Atenció, aquesta directiva s’indica en recursos que són de lectura / escriptura i serveix per restringir aquests usuaris atorgant-los només el dret de lectura (i privant-los del de escriptura).

```
[public]
   	 comment = Share de contingut public
   	 path = /var/lib/samba/public
   	 browseable = yes
   	 writable = yes
         guest ok = yes
         read list = pla
```

L’usuari pla només pot llistar.
```
root@client:/opt/docker# smbclient -U roc%roc //samba/public
Try "help" to get a list of possible commands.

smb: \> ls
  .                               	D    	0  Mon Mar 18 15:35:51 2024
  ..                              	D    	0  Mon Mar 18 15:07:58 2024
  smb.conf                        	N 	8842  Mon Mar 18 15:07:58 2024
  nobody.txt                      	A 	8842  Mon Mar 18 15:35:51 2024
  roc.txt                         	A  	389  Mon Mar 18 15:27:05 2024
  treball.txt                     	A  	389  Mon Mar 18 15:12:15 2024
  startup.sh                      	N  	388  Mon Mar 18 15:07:58 2024
  REAMDE.md                       	N 	2764  Mon Mar 18 15:07:58 2024
  Dockerfile                      	N  	389  Mon Mar 18 15:07:58 2024

   	 479079112 blocks of size 1024. 395151856 blocks available

smb: \> rm nobody.txt
smb: \> exit

root@client:/opt/docker# smbclient -U pla //samba/public
Password for [WORKGROUP\pla]:
Try "help" to get a list of possible commands.

smb: \> ls
  .                               	D    	0  Mon Mar 18 15:40:46 2024
  ..                              	D    	0  Mon Mar 18 15:07:58 2024
  smb.conf                        	N 	8842  Mon Mar 18 15:07:58 2024
  roc.txt                         	A  	389  Mon Mar 18 15:27:05 2024
  treball.txt                     	A  	389  Mon Mar 18 15:12:15 2024
  startup.sh                      	N  	388  Mon Mar 18 15:07:58 2024
  REAMDE.md                       	N 	2764  Mon Mar 18 15:07:58 2024
  Dockerfile                      	N  	389  Mon Mar 18 15:07:58 2024

   	 479079112 blocks of size 1024. 395152056 blocks available

smb: \> rm treball.txt
NT_STATUS_ACCESS_DENIED deleting remote file \treball.txt

smb: \> put Dockerfile pla.txt
NT_STATUS_ACCESS_DENIED opening remote file \pla.txt
```


#### Exemple 11
**Llista d’usuaris autoritzats per a escriptura**
Indica la llista d’usuaris amb dret d’escriptura al recurs. S’utilitza en recursos que són read only per a tots els usuaris però es permet als usuaris indicats a la llsita el dret de escriptura.

```
[public]
   	 comment = Share de contingut public
   	 path = /var/lib/samba/public
   	 browseable = yes
   	 writable = no
         guest ok = yes
         write list = pla
```

Com a pla hem pogut esborrar contingut, perquè tenim la directiva write list = yes.
```
root@client:/opt/docker# smbclient -U lila%lila //samba/public
Try "help" to get a list of possible commands.

smb: \> ls
  .                               	D    	0  Mon Mar 18 15:40:46 2024
  ..                              	D    	0  Mon Mar 18 15:07:58 2024
  smb.conf                        	N 	8842  Mon Mar 18 15:07:58 2024
  roc.txt                         	A  	389  Mon Mar 18 15:27:05 2024
  treball.txt                     	A  	389  Mon Mar 18 15:12:15 2024
  startup.sh                      	N  	388  Mon Mar 18 15:07:58 2024
  REAMDE.md                       	N 	2764  Mon Mar 18 15:07:58 2024
  Dockerfile                      	N  	389  Mon Mar 18 15:07:58 2024

   	 479079112 blocks of size 1024. 395148844 blocks available

smb: \> rm treball.txt
NT_STATUS_ACCESS_DENIED deleting remote file \treball.txt
smb: \> exit

root@client:/opt/docker# smbclient -U pla //samba/public
Password for [WORKGROUP\pla]:
Try "help" to get a list of possible commands.

smb: \> rm treball.txt
```


#### Exemple 12
**Modes de directori i fitxer**
create mode = mode
directory mode = mode
Permeten establir el mode dels directoris i els fitxers de nova creació dins el share.

```
[public]
   	 comment = Share de contingut public
   	 path = /var/lib/samba/public
   	 browseable = yes
   	 writable = yes
         guest ok = yes
         create mode = 0660
         directory mode = 0770
```

```
root@client:/opt/docker# smbclient -U roc%roc //samba/public
Try "help" to get a list of possible commands.

smb: \> mkdir newdir
smb: \> put Dockerfile newfile.txt
putting file Dockerfile as \newfile.txt (379.8 kb/s) (average 379.9 kb/s)
smb: \>
```

Comprovem que els permisos han canviat als que teniem per defecte.
```
root@samba:/opt/docker# ls -la /var/lib/samba/public/
total 48
drwxrwxrwx 3 root root 4096 Mar 18 15:52 .
drwxr-xr-x 1 root root 4096 Mar 18 15:07 ..
-rw-r--r-- 1 root root  389 Mar 18 15:07 Dockerfile
-rw-r--r-- 1 root root 2764 Mar 18 15:07 REAMDE.md
drwxrwx--- 2 roc  roc  4096 Mar 18 15:51 newdir
-rw-rw---- 1 roc  roc   389 Mar 18 15:52 newfile.txt
-rwxr--r-- 1 root roc   389 Mar 18 15:27 roc.txt
-rw-r--r-- 1 root root 8842 Mar 18 15:07 smb.conf
-rwxr-xr-x 1 root root  388 Mar 18 15:07 startup.sh
```




