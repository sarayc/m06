#! /bin/bash
# @sarayc ASIX-M06
# -------------------------------------
# Share public
mkdir -p /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.

# Share privat
mkdir -p /var/lib/samba/privat
cp /etc/os-release /var/lib/samba/privat/.

# copiar la configuració
cp /opt/docker/smb.conf /etc/samba/smb.conf

for user in lila roc patipla pla
do
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | smbpasswd -a $user
done


/usr/sbin/smbd 
/usr/sbin/nmbd -F
