## SAMBA
```
docker run --rm --name samba.edt.org -h samba.edt.org --net 2hisx --privileged -d sarayj03/samba23:base
```

### Fitxers:
+ Dockerfile
+ startup.sh
+ smb.conf

**Dockerfile**
```
# ldapserver
FROM debian:latest
LABEL version="1.0"
LABEL author="@sarayc Curs 2023-2024"
LABEL subject="SAMBA Server"
RUN apt-get update
#ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -y install procps samba vim procps nmap tree iproute2 samba-client cifs-utils
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
```




**startup.sh**
```
#! /bin/bash
# -------------------------------------

# Share public
mkdir /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.

# Share privat
mkdir /var/lib/samba/privat
#chmod 777 /var/lib/samba/privat
cp /opt/docker/*.md /var/lib/samba/privat/.

# Configuració samba
cp /opt/docker/smb.conf /etc/samba/smb.conf

# Creació usuaris unix/samba
for user in lila roc patipla pla
do	
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | smbpasswd -a $user
done
pdbedit -L

# Activar els serveis
/usr/sbin/smbd && echo "smb Ok"
/usr/sbin/nmbd -F && echo "nmb  Ok"
```




**smb.conf**
```
[global]
        workgroup = MYGROUP
        server string = Samba Server Version %v
        log file = /var/log/samba/log.%m
        max log size = 50
        security = user
        passdb backend = tdbsam
        load printers = yes
        cups options = raw
[homes]
        comment = Home Directories
        browseable = no
        writable = yes
;       valid users = %S
;       valid users = MYDOMAIN\%S
[printers]
        comment = All Printers
        path = /var/spool/samba
        browseable = no
        guest ok = no
        writable = no
        printable = yes

# ------------------------------------------------------------------
# SHARES d'exemple M06
# -------------------------------------------------------------------
# guest ok = yes  <--> public = yes
# read only = yes <--> writable = no
[documentation]
    comment = Documentació doc del container 
    path = /usr/share/doc
    public = yes
    browseable = yes
    writable = no
[manpages]
    comment = Documentació man  del container 
    path = /usr/share/man
    guest ok = yes
    browseable = yes
    read only = yes
[public]
        comment = Share de contingut public
        path = /var/lib/samba/public
        public = yes
        browseable = yes
        writable = yes
        printable = no
        guest ok = yes
[privat]
        comment = Share d'accés privat
        path = /var/lib/samba/privat
        public = no
        browseable = no
        writable = yes
        printable = no
        guest ok = yes
```



### 
