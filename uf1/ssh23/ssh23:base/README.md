## ssh base

### Maneres d'engegar el container
#### 1) docker-compose.yml
```
version: "2:"
services:
  ldap:
    image: sarayj03/ldap23:latest
    container_name: ldap.edt.org
    hostname: ldap.edt.org
    ports:
      - "389:389"
    networks:
      - 2hisx
  ssh:
    image: sarayj03/ssh23:base
    privileged: true
    container_name: ssh.edt.org
    hostname: ssh.edt.org
    ports:
      - 2022:20
    networks:
      - 2hisx
networks:
  2hisx:
```

#### 2) Engegar amb ordres
```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -d edtasixm06/ldap22:latest
```

```
docker run --rm --name ssh.edt.org -h ssh.edt.org --net 2hisx --privileged -p 2022:20 -d edtasixm06/ssh22:base
```



### Fitxers:
+ Dockerfile
+ startup.sh
+ nsswitch.conf
+ nslcd.conf
+ common-session
+ ldap.conf
+ startup.sh


### Comprovacions
#### Comprovem que podem accedir amb usuaris unix
```
unix01@ssh:~$ su -l unix02
Password: 
unix02@ssh:~$ 
```


#### Comprovem que podem accedir amb usuaris LDAP
```
unix02@ssh:~$ su -l marta
Password: 
Creating directory '/tmp/home/marta'.
$ bash
marta@ssh:~$ id
uid=5003(marta) gid=600(alumnes) groups=600(alumnes)
marta@ssh:~$ pwd
/tmp/home/marta
```


#### Comprovem que tenim el port propagat
2022:20 --> port host:port container

```
saray@debian:~/m06/ssh/ssh23:base$ docker ps
CONTAINER ID   IMAGE                    COMMAND                  CREATED         STATUS         PORTS                                   NAMES
5cedee277647   sarayj03/ssh23:base      "/bin/sh -c /opt/doc…"   4 minutes ago   Up 4 minutes   0.0.0.0:2022->20/tcp, :::2022->20/tcp   ssh.edt.org
c59ef061bef2   sarayj03/ldap23:latest   "/bin/sh -c /opt/doc…"   4 minutes ago   Up 4 minutes   0.0.0.0:389->389/tcp, :::389->389/tcp   ldap.edt.org
```

### Proves ssh
**fingerprint**

Ententem entrar com a Ramon, ens pregunta el fingerprint la primera vegada, la segona vegada no ens ho demana perquè s’ho ha guardat al fitxer .ssh/knonw_hosts.

```
saray@debian:~/m06/ssh/ssh23:base$ ssh ramon@172.24.0.2
The authenticity of host '172.24.0.2 (172.24.0.2)' can't be established.
ECDSA key fingerprint is SHA256:74A1tezASFrTXM2TfzGsSANwIcpeokQJ7Eb8B2tipWQ.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '172.24.0.2' (ECDSA) to the list of known hosts.
ramon@172.24.0.2's password: 

saray@debian:~/m06/ssh/ssh23:base$ ssh ramon@172.24.0.2
ramon@172.24.0.2's password: 
```

**canvia el fingerprint**
Editem el fitxer /etc/hosts, on afegim l’última línea, on assigna la direcció 127.0.0.1 al nom de host bilbao.edt.org. Quan intentem accedir a bilbao.edt.org des d’aquest sistema es resoldrà la IP 127.0.0.1, ja que, és l’adreça IP local del propi sistema.

```
root@ssh:/opt/docker# cat /etc/hosts
127.0.0.1    localhost
::1    localhost ip6-localhost ip6-loopback
fe00::0    ip6-localnet
ff00::0    ip6-mcastprefix
ff02::1    ip6-allnodes
ff02::2    ip6-allrouters
172.24.0.2    ssh.edt.org ssh
127.0.0.1 bilbao.edt.org
```

Comprovem amb nmap si tenim el port obert.
```
root@ssh:/opt/docker# nmap bilbao.edt.org
Starting Nmap 7.93 ( https://nmap.org ) at 2024-02-07 04:51 UTC
Nmap scan report for bilbao.edt.org (127.0.0.1)
Host is up (0.0000040s latency).
rDNS record for 127.0.0.1: localhost
Not shown: 999 closed tcp ports (reset)
PORT   STATE SERVICE
22/tcp open  ssh

Nmap done: 1 IP address (1 host up) scanned in 0.11 seconds
```

Ara intentem accedir amb ssh
```
root@ssh:/opt/docker# ssh ramon@bilbao.edt.org
The authenticity of host 'bilbao.edt.org (127.0.0.1)' can't be established.
ED25519 key fingerprint is SHA256:gmBV+aTgKO+ETAEFcFoOq/K0JhNKTDGjV76wxikfRpo.
This key is not known by any other names.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'bilbao.edt.org' (ED25519) to the list of known hosts.
ramon@bilbao.edt.org's password:
```

Tornem a editar el fitxer /etc/hosts 
```
root@ssh:/opt/docker# cat /etc/hosts
127.0.0.1    localhost
::1    localhost ip6-localhost ip6-loopback
fe00::0    ip6-localnet
ff00::0    ip6-mcastprefix
ff02::1    ip6-allnodes
ff02::2    ip6-allrouters
172.24.0.2    ssh.edt.org ssh
172.20.0.1 bilbao.edt.org
```

Ara si ens intentem connectar a un host que ja ho te identificat no ens deixa entrar perquè ens diu que ha canviat el fingerprint, però ens diu el que podem fer, pot haver passa:

+ algú ha intentat entrar
+ el servidor ha canviat d’identitat, de claus…i per tant ja ho sabiem
+ o, pot ser depèn de la configuració del client, pot estar en mode estricte o mode promiscu (que entra qui vulgui, ens donaria l’avís però podriem continuar, i per tant, entrar).

Podem esborrar l’entrada del fingerpint.
```
root@ssh:/opt/docker# ssh ramon@bilbao.edt.org
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@	WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED! 	@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that a host key has just been changed.
The fingerprint for the ED25519 key sent by the remote host is
SHA256:FyEIqfKp1l24TVvD742V8a7k5J5+JSgwsHvlei6JNwc.
Please contact your system administrator.
Add correct host key in /root/.ssh/known_hosts to get rid of this message.
Offending ED25519 key in /root/.ssh/known_hosts:1
  remove with:
  ssh-keygen -f "/root/.ssh/known_hosts" -R "bilbao.edt.org"
Host key for bilbao.edt.org has changed and you have requested strict checking.
Host key verification failed.
```


### Configuració client
**Exemple 1**
Intenta establi una connexió ssh al host local localhost, amb l’usuari unix01, on desactivem l’autenticació mitjançant contrasenya i mostrant una representació visual de la calu de host durant la connexió.

```
root@ssh:/opt/docker# ssh -o VisualHostKey=yes -o PasswordAuthentication=no unix01@localhost
The authenticity of host 'localhost (127.0.0.1)' can't be established.
ED25519 key fingerprint is SHA256:gmBV+aTgKO+ETAEFcFoOq/K0JhNKTDGjV76wxikfRpo.
+--[ED25519 256]--+
|B*+ o...     	|
|.O++. . .    	|
|+.Oo.. +     	|
|=O.=.o. .    	|
|E*O o . S    	|
|=Xoo   .     	|
|+o=          	|
| +.          	|
|             	|
+----[SHA256]-----+
This key is not known by any other names.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'localhost' (ED25519) to the list of known hosts.
unix01@localhost: Permission denied (publickey,password).
```

**Exemple 2**
Editem el fitxer /etc/ssh/ssh_config
```
PasswordAuthentication no
VisualHostKey yes
```


Si intentem accedir.
```
root@ssh:/opt/docker# ssh unix01@localhost
Host key fingerprint is SHA256:gmBV+aTgKO+ETAEFcFoOq/K0JhNKTDGjV76wxikfRpo
+--[ED25519 256]--+
|B*+ o...     	|
|.O++. . .    	|
|+.Oo.. +     	|
|=O.=.o. .    	|
|E*O o . S    	|
|=Xoo   .     	|
|+o=          	|
| +.          	|
|             	|
+----[SHA256]-----+
unix01@localhost: Permission denied (publickey,password).
```


Però i si ho passem com a opció, que agafarà?
```
root@ssh:/opt/docker# ssh -o VisualHostKey=no unix01@localhost
unix01@localhost: Permission denied (publickey,password).
```


### Configuració accés ssh desatès per clau pública
Exemple de connexió desatesa, on Ramon pugui entrar automàticament al servidor. El Ramon pot tenir tantes claus públiques com vulgui. 
Al docker-compose tenim posat que propagi el port 22 del container en el 2022 del meu ordinador, a totes les interfícies. Perquè com no hem posat res, per defecte és 0.0.0.0, és a dir, que el servidor estarà disponible per accedir des de qualsevol interfície de xarxa al nostra sistema. 

Ens hem de connectar al port 2022.
Volem crear un parell de claus, de tipus RSA. Des del nostra ordinador, anem al directori .ssh i creem un parell de claus amb l’ordre ssh-keygen.

```
saray@debian:~/.ssh$ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/home/saray/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/saray/.ssh/id_rsa
Your public key has been saved in /home/saray/.ssh/id_rsa.pub
The key fingerprint is:
SHA256:WKLUnSNL7Sz+SMB2O9J2K6db27VuvTXLerjwTYaofzQ saray@debian
The key's randomart image is:
+---[RSA 3072]----+
|             	|
| 	. o .   	|
|	. = *    	|
|   o o O .   	|
|	= = S    	|
|   . = o	.E.  |
|	. B o  oooo+.|
| 	+.*.+..++*oo|
|  	+=+.o+++== |
+----[SHA256]-----+
```

```
saray@debian:~/.ssh$ ls -la
total 44
drwxrwxrwx  2 saray saray 4096 Feb  7 06:29 .
drwxr-xr-x 31 saray saray 4096 Feb  7 05:36 ..
-rw-------  1 saray saray 2602 Feb  7 06:29 id_rsa
-rw-r--r--  1 saray saray  566 Feb  7 06:29 id_rsa.pub
-rw-------  1 saray saray 1674 Nov 22 05:33 keypair-proyecto1.pem
-r--------  1 saray saray 1678 Nov 24 18:34 keypair-wordpress.pem
-rw-------  1 saray saray 1674 Dec 10 12:41 keypari-nova.pem
-rw-r--r--  1 saray saray 4218 Feb  7 05:43 known_hosts
-rw-r--r--  1 saray saray 3552 Dec 10 12:27 known_hosts.old
-rw-------  1 saray saray 1679 Dec  5 18:14 vockey.pem
```

Necessitem copiar la clau pública a dins del container en el servidor ssh.
Entrem al container i ens convertim en pere, accedir al directori ssh però no en tenim cap perquè encara no hem fet res amb ssh.

Des de la nostra màquina, fem l’ordre ssh-copy-id

```
saray@debian:~/.ssh$ ssh-copy-id -i /home/saray/.ssh/id_rsa.pub pere@172.24.0.2
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/home/saray/.ssh/id_rsa.pub"
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
pere@172.24.0.2's password:

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh 'pere@172.24.0.2'"
and check to make sure that only the key(s) you wanted were added.
```

Ara si tornem al container com a Pere observem que tenim un fitxer authorized_keys que conté la clau pública que permet entrar com a usuari pere en aquest ordinador. 
```
pere@ssh:~$ ls -l .ssh/authorized_keys  
-rw------- 1 pere users 566 Feb  7 05:34 .ssh/authorized_keys

pere@ssh:~$ cat .ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDbVYaZgDHJmZZYyzHlDO4InE/T0LvdmYSoejMdfmYV2iMsH5JJi/ZGYZBIad8GXqIeCUIoTs0FZ7jKmiWwxECziD8JlG+69MISquDojP4CXt9NISgKiKUVPW4BuxulD2ITP6FdsovDyUlIUF02BKh/1RaeATTONUMIOVS7WYp6KQx3Fu8U7lxAfadGeJf6jAJhuTfg0AVqDMW6QDfdGFt1LdaM+tdBTQqS9dSKi5irkj8JHqbN8dGIF5jGIrK4bqlzp8sBSJ4tWv06LIyOB1uDYpr8hommxsopISwWiovpsXUqtcbVxSNB5rmE+LNF74dkLQJgmDyteCjT/nKCZpRsgBVFMXyNQJlkpOX8bgV62QwKno1BzH1MF5WYQWIqKvB1uBiRTeD91DrifoiXgaTO3ou2Bq61jFQ5QuIMXojBtMaN6fuyWRv9OUMRn2cvDF5yjjQRSmcAaaggIHr24YDGJhMeVb/EqATST7OCXkscILxCMecKrY6wP30HeiIvRz0= saray@debian
```

A la nostra màquina podrem entrar sense contrasenya perquè entra amb la public key.
```
saray@debian:~/.ssh$ ssh pere@172.24.0.2
Linux ssh.edt.org 5.10.0-22-amd64 #1 SMP Debian 5.10.178-3 (2023-04-22) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Wed Feb  7 05:23:34 2024 from 172.24.0.1
$ exit
```

Com usuaris creem una altra clau pública amb un altra tipus de tecnologia, i li posem una passphrase “secret”.
```
saray@debian:~/.ssh$ ssh-keygen -t ecdsa
Generating public/private ecdsa key pair.
Enter file in which to save the key (/home/saray/.ssh/id_ecdsa): tinder
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in tinder
Your public key has been saved in tinder.pub
The key fingerprint is:
SHA256:0kDD3CHOoNG+JXm1ZC9ryifCVtalcgmQsNVXMBktG3k saray@debian
The key's randomart image is:
+---[ECDSA 256]---+
|  ooo=+.=O.  	|
|   =+=+o@.E  	|
|  o. o+= B   	|
|	+ +o+ o  	|
| 	=.oS*   	|
|	. +.B    	|
|   . + =     	|
|	+ + .    	|
|   . . o     	|
+----[SHA256]-----+
```

Copiem aquesta clau a Anna
```
a211464sc@i07:~/.ssh$ ssh-copy-id -i /home/users/inf/hisx2/a211464sc/.ssh/id_rsa.pub anna@172.26.0.3
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/home/users/inf/hisx2/a211464sc/.ssh/id_rsa.pub"
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
anna@172.26.0.3's password:

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh 'anna@172.26.0.3'"
and check to make sure that only the key(s) you wanted were added.
```


Entrem com a anna
Si entrem com a anna per ssh, ens a demanat la passphrase, si no la posem ens demanarà la contrasenya.
```
a211464sc@i07:~/.ssh$ ssh anna@172.26.0.3
Linux ssh.edt.org 6.1.0-9-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.1.27-1 (2023-05-08) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Wed Feb  7 11:00:07 2024 from 172.26.0.1
$ exit
Connection to 172.26.0.3 closed.
```


### Configuració servidor
El fitxer de configuració /etc/ssh/sshd-conf. Posem que volem pel port 2022.

**Exemple 1**
```
Port 2022
#AddressFamily any
ListenAddress 0.0.0.0
#ListenAddress ::
```

Reiniciem el servei amb kill -1 
Ara si fem un nmap localhost tenim el port 2022 obert.

```
root@ssh:/opt/docker# nmap localhost
Starting Nmap 7.93 ( https://nmap.org ) at 2024-02-07 11:18 UTC
Nmap scan report for localhost (127.0.0.1)
Host is up (0.0000030s latency).
Other addresses for localhost (not scanned): ::1
Not shown: 999 closed tcp ports (reset)
PORT 	STATE SERVICE
2022/tcp open  down

Nmap done: 1 IP address (1 host up) scanned in 0.13 seconds
```

Ara si volem entrar com a root del container per exemple. Per poder fer aixó, canviem la contrasenya de root. Per poder entrar ara haurem de posar el port 2022, però no ens podem connectar, perquè es accessible des de dins però no des de fora. Per poder entrar, hem de posar al fitxer.

```
PermitRootLogin yes
```

```
a211464sc@i07:~/.ssh$ ssh -p 2022 root@172.28.0.3
root@172.28.0.3's password:
Linux ssh.edt.org 6.1.0-9-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.1.27-1 (2023-05-08) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
root@ssh:~# exit
```

**Exemple 2**
Podem posar un altra cop el port 22. Editem el fitxer, afegim aquestes directives. Creem un fitxer /etc/banner i posem un missatge, que serà el missatge que se’ns mostrarà.

```
Match User pau
    banner /etc/banner
    PasswordAuthentication no
```

Comprovem que com a Pau no podem entrar
```
a211464sc@i07:~/.ssh$ ssh pau@172.28.0.3
hola
pau@172.28.0.3: Permission denied (publickey).
```

Com a Marta i amb qualsevol usuari podem entrar.
```
a211464sc@i07:~/.ssh$ ssh marta@172.28.0.3
marta@172.28.0.3's password:
Linux ssh.edt.org 6.1.0-9-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.1.27-1 (2023-05-08) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Wed Feb  7 11:31:57 2024 from 172.28.0.1
$
```

**Exemple 3**

Posem a /etc/ssh/sshd_config 

```
AllowUsers unix02 marta
```

Només podrà entrar unix02 i marta
```
a211464sc@i07:~/.ssh$ ssh unix02@172.28.0.3
unix02@172.28.0.3's password:
Linux ssh.edt.org 6.1.0-9-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.1.27-1 (2023-05-08) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
unix02@ssh:~$ exit
logout
Connection to 172.28.0.3 closed.

a211464sc@i07:~/.ssh$ ssh unix01@172.28.0.3
unix01@172.28.0.3's password:
Permission denied, please try again.
unix01@172.28.0.3's password:
```
