## SSH phpldapadmin

+ docker-compose.yml

```
version: "3.8"
services:
  ldap:
    image: sarayj03/ldap23:latest
    container_name: ldap.edt.org
    hostname: ldap.edt.org
    ports:
      - "389:389"
    networks:
      - 2hisx
  ssh:
    image: sarayj03/ssh23:base
    privileged: true
    container_name: ssh.edt.org
    hostname: ssh.edt.org
    ports:
      - 2022:20
    networks:
      - 2hisx
  phpldapadmin:
    image: sarayj03/phpldapadmin
    container_name: phpldapadmin
    hostname: phpldapadmin
    ports:
      - "80:80"
    networks:
      - 2hisx

networks:
  2hisx:
```



### Accedir a phpldapadmin
Al navegador posem "localhost/phpldapadmin"


<img src="imatges/imatge1.png">


Ens acreditem com a anonymous


<img src="imatges/imatge2.png">



### Usuaris locals i ldap
```
root@ssh:/opt/docker# getent passwd unix01
unix01:x:1000:1000::/home/unix01:/bin/bash

root@ssh:/opt/docker# getent passwd marta
marta:*:5003:600:Marta Mas:/tmp/home/marta:
```



