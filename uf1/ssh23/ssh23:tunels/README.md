## ssh23 túnels
### Pràctica túnel directe

**Abans de començar la pràctica, comprovem que funciona la imatge ldap i phpldapadmin**
Engegem dos containers, un ldap i un phpldapadmin. 
Tenim un docker compose amb un ldap, ssh i phpldapadmin, podem engegar aquest compose o per ordres de comandes.

**docker compose**

```
version: "3.8"
services:
  ldap:
	image: sarayj03/ldap23:latest
	container_name: ldap.edt.org
	hostname: ldap.edt.org
	ports:
  	- "389:389"
	networks:
  	- 2hisx
  ssh:
	image: sarayj03/ssh23:base
	privileged: true
	container_name: ssh.edt.org
	hostname: ssh.edt.org
	ports:
  	- 2022:20
	networks:
  	- 2hisx
  phpldapadmin:
	image: sarayj03/phpldapadmin
	container_name: phpldapadmin
	hostname: phpldapadmin
	ports:
  	- "80:80"
	networks:
  	- 2hisx

networks:
  2hisx:
```


**Comandes**
```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -d sarayj03/ldap23:latest
```

```
docker run --rm --name phpldapadmin -h phpldapadmin --net 2hisx -d sarayj03/phpldapadmin
```


Comprovem des del navegador que funciona.
```
localhost/phpldapadmin
```


<img src="imatges/imatge1.png">



Ens acreditem com a anonymous


<img src="imatges/imatge2.png">



---------------------------------------------------------------------------------------------


## Pràctica túnel directe
Engegem un phpldapadmin a la nostra màquina local, i un ldap a una màquina d'Amazon.


**Container phpldapadmin**
```
docker run --rm --name phpldapadmin -h phpldapadmin --net 2hisx --d sarayj03/phpldapadmin
```


**Màquina Amazon**
```
saray@saray-XPS-13-9360:~/m06/ssh/ssh23:tunels$ ssh -i /home/saray/vockey.pem admin@34.207.94.11
```

A la màquina Amazon ens engegem un ldap.
```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -d sarayj03/ldap23:latest
```




### Creació túnel
Entrem al container phpldapadmin i ens instal·len el paquet ssh client per poder realitzar ordres ssh, també cal que copiem la clau pública de la màquina de Amazon.


1) instalar paquet

```
docker exec -it phpldapadmin /bin/bash
yum install openssh-clients
```


2) copiar la clau
Podem copiar la clau amb un docker cp o manualment.
Instal·lem altres utilitats.

```
yum install vim nmap
```

Recordem canviar els permisos de la clau a 400.


3) Creem el túnel

```
[root@phpldapadmin docker]# ssh -i vockey.pem -L 5001:localhost:389 admin@34.207.94.11
bind: Cannot assign requested address
Linux ip-172-31-90-28 6.1.0-13-cloud-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.1.55-1 (2023-09-29) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Wed Feb 21 16:30:25 2024 from 46.16.39.239
```


4) Realitzem un nmap localhost

```
[root@phpldapadmin docker]# nmap localhost

Starting Nmap 7.60 ( https://nmap.org ) at 2024-02-21 16:30 UTC
Nmap scan report for localhost (127.0.0.1)
Host is up (0.0000060s latency).
Other addresses for localhost (not scanned): ::1
Not shown: 998 closed ports
PORT     STATE SERVICE
80/tcp   open  http
5001/tcp open  commplex-link

Nmap done: 1 IP address (1 host up) scanned in 1.68 seconds
```


**Important** 
Estem obrint el port 5001 però a la configuració de phpldapadmin tenim el 389, per tant, no podrem accedir al phpldapadmin, si volem accedir, hem de canviar aquest port i el nom per:

```
vim /etc/phpldapadmin/config.php
```

+ localhost
+ 5001








-------------------------------------------------------------------------------------------------

## Pràctica túnel invers

Engegem un ldap a la nostra màquina local. A la màquina Amazon tenim un container phpldapadmin i important els security groups només hem de te tenir el port 22 i 80 engegat. 

**Container ldap**

```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -d sarayj03/ldap23:latest
```

**Màquina Amazon**

```
ssh -i vockey.pem admin@<ip container>
```

Engegem container phpldapadmin a la màquina Amazon

```
docker run --rm --name phpldapadmin -h phpldapadmin --net 2hisx -p 80:80 -d sarayj03/phpldapadmin
```



### Creació túnel invers
```
ssh -i vockey.pem -R 5001:localhost:389 admin@<ip container>
```

Comprovem que si fem nmap localhost a la màquina Amazon, tenim el port 5001 obert.

```

```


Podem fer un ldapsearch, instal·lant el paquet necessari per utilitzar ordres client

```
sudo apt-get install ldap-utils
```

```
ldapsearch -x -LLL -H ldap://localhost:5001 -b 'dc=edt,dc=org'
```
