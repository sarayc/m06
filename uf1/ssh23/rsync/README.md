# pràctica rsync

Rsync vol dir "sincronització remota", és una eina de sincronització de fitxers remots i locals. 
Utilitza un algorisme que minimitza la quantitat de dades copiades, movent només les parts dels fitxers que van canviar.

Creem dos directoris de prova i alguns fitxers amb les següents ordres:

```
a211464sc@i07:~/m06/ssh23/ssh23:rsync$ mkdir dir1
a211464sc@i07:~/m06/ssh23/ssh23:rsync$ mkdir dir2

a211464sc@i07:~/m06/ssh23/ssh23:rsync$ touch dir1/file{1..100}
```

Tenm un directori anomenat dir1 amb 100 fitxers buits.

```
a211464sc@i07:~/m06/ssh23/ssh23:rsync$ ls dir1
file1	file14  file2   file25  file30  file36  file41  file47  file52  file58  file63  file69  file74  file8   file85  file90  file96
file10   file15  file20  file26  file31  file37  file42  file48  file53  file59  file64  file7   file75  file80  file86  file91  file97
file100  file16  file21  file27  file32  file38  file43  file49  file54  file6   file65  file70  file76  file81  file87  file92  file98
file11   file17  file22  file28  file33  file39  file44  file5   file55  file60  file66  file71  file77  file82  file88  file93  file99
file12   file18  file23  file29  file34  file4   file45  file50  file56  file61  file67  file72  file78  file83  file89  file94
file13   file19  file24  file3   file35  file40  file46  file51  file57  file62  file68  file73  file79  file84  file9   file95
```

També tenim un directori buit anomenat dir2.
Per sincronizar el contingut dir1 a dir2 al mateix sistema realitzem.

+  -r ens serveix per copiar de manera recuriva
+ si li posem la / a dir1/ ens copiarà el contingut del directori
+ si posem dir1 sense / ens agafa el directori i el seu contingut

```
a211464sc@i07:~/m06/ssh23/ssh23:rsync$ rsync -r dir1/ dir2

a211464sc@i07:~/m06/ssh23/ssh23:rsync$ ls dir2
file1	file14  file2   file25  file30  file36  file41  file47  file52  file58  file63  file69  file74  file8   file85  file90  file96
file10   file15  file20  file26  file31  file37  file42  file48  file53  file59  file64  file7   file75  file80  file86  file91  file97
file100  file16  file21  file27  file32  file38  file43  file49  file54  file6   file65  file70  file76  file81  file87  file92  file98
file11   file17  file22  file28  file33  file39  file44  file5   file55  file60  file66  file71  file77  file82  file88  file93  file99
file12   file18  file23  file29  file34  file4   file45  file50  file56  file61  file67  file72  file78  file83  file89  file94
file13   file19  file24  file3   file35  file40  file46  file51  file57  file62  file68  file73  file79  file84  file9   file95
```

### Prova 1: local-local
Canviem el nom de file1 per a saray al directori dir1.

Aquesta ordre no ens realitzarà cap canvi, perquè tenim la -n.
+ -n ens realitza un testing, i no s'aplica canvis.
+ -a ens serveix per sincrontizar.
+ -v ens va un verbose dels fitxers que realitzarà canvis.

```
a211464sc@i07:~/m06/ssh23/ssh23:rsync$ rsync -anv dir1/ dir2
sending incremental file list
./
saray

sent 1.361 bytes  received 22 bytes  2.766,00 bytes/sec
total size is 0  speedup is 0,00 (DRY RUN)
```


Apliquem els canvis

```
a211464sc@i07:~/m06/ssh23/ssh23:rsync$  rsync -av dir1/ dir2
sending incremental file list
./
saray

sent 1.401 bytes  received 38 bytes  2.878,00 bytes/sec
total size is 0  speedup is 0,00
```


Podem comprovar que ens ha modificar el nom del fitxer al directori dir2

```
a211464sc@i07:~/m06/ssh23/ssh23:rsync$ ls dir2
file1	file14  file2   file25  file30  file36  file41  file47  file52  file58  file63  file69  file74  file8   file85  file90  file96
file10   file15  file20  file26  file31  file37  file42  file48  file53  file59  file64  file7   file75  file80  file86  file91  file97
file100  file16  file21  file27  file32  file38  file43  file49  file54  file6   file65  file70  file76  file81  file87  file92  file98
file11   file17  file22  file28  file33  file39  file44  file5   file55  file60  file66  file71  file77  file82  file88  file93  file99
file12   file18  file23  file29  file34  file4   file45  file50  file56  file61  file67  file72  file78  file83  file89  file94  saray
file13   file19  file24  file3   file35  file40  file46  file51  file57  file62  file68  file73  file79  file84  file9   file95
```



### Prova 2: local-remot
Engegem un docker compose que té un ssh i un ldap, entre al container ssh i instal·lem rsync.

```
version: "2"
services:
  ldap:
	image: sarayj03/ldap23:latest
	container_name: ldap.edt.org
	hostname: ldap.edt.org
	ports:
  	- "389:389"
	networks:
  	- 2hisx
  ssh:
	image: sarayj03/ssh23:base
	privileged: true
	container_name: ssh.edt.org
	hostname: ssh.edt.org
	ports:
  	- 2022:20
	networks:
  	- 2hisx
networks:
  2hisx:
```

```
apt-get install rsync
```



Realitzem l'ordre per poder copiar els fitxers que tenim a dir1 de manera local a l'usuari pere remot.

```
a211464sc@i07:~/m06/ssh23/ssh23:rsync$ rsync -a dir1/ pere@172.26.0.2:/tmp/home/pere/.
The authenticity of host '172.18.0.2 (172.18.0.2)' can't be established.
ED25519 key fingerprint is SHA256:GVnBPJGRGamH+DBGal6/pIoaO79Glz7ig+4bmcfsDtI.
This host key is known by the following other names/addresses:
	~/.ssh/known_hosts:71: [hashed name]
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '172.18.0.2' (ED25519) to the list of known hosts.
pere@172.18.0.2's password:
```


Anem la container i comprovem

```
pere@ssh:~$ ls
file10   file15  file20  file26  file31  file37  file42  file48  file53  file59  file64  file7   file75  file80  file86  file91  file97
file100  file16  file21  file27  file32  file38  file43  file49  file54  file6   file65  file70  file76  file81  file87  file92  file98
file11   file17  file22  file28  file33  file39  file44  file5   file55  file60  file66  file71  file77  file82  file88  file93  file99
file12   file18  file23  file29  file34  file4   file45  file50  file56  file61  file67  file72  file78  file83  file89  file94  saray
file13   file19  file24  file3   file35  file40  file46  file51  file57  file62  file68  file73  file79  file84  file9   file95
file14   file2   file25  file30  file36  file41  file47  file52  file58  file63  file69  file74  file8   file85  file90  file96
```




### Prova 3: remot-local
Creem a Anna 10 fitxers buits dins del container

```
pere@ssh:~$ su -l anna
Password:
$ bash
anna@ssh:~$ touch fitxers_anna{1..10}
anna@ssh:~$
```


Des de la nostra màquina local fem

```
a211464sc@i07:~/m06/ssh23/ssh23:rsync$ rsync -a anna@172.26.0.2:/tmp/home/anna/ .
anna@172.26.0.2's password:
```


Comprovem que se'ns han copiat els fitxers

```
a211464sc@i07:~/m06/ssh23/ssh23:rsync$ ls -l
total 16
drwxr-xr-x 2 a211464sc hisx2 4096 feb 22 12:10 dir1
drwxr-xr-x 2 a211464sc hisx2 4096 feb 22 12:10 dir2
-rw-r--r-- 1 a211464sc hisx2  373 feb 22 12:20 docker-compose.yml
-rw-r--r-- 1 a211464sc hisx2	0 feb 22 12:39 fitxers_anna1
-rw-r--r-- 1 a211464sc hisx2	0 feb 22 12:39 fitxers_anna10
-rw-r--r-- 1 a211464sc hisx2	0 feb 22 12:39 fitxers_anna2
-rw-r--r-- 1 a211464sc hisx2	0 feb 22 12:39 fitxers_anna3
-rw-r--r-- 1 a211464sc hisx2	0 feb 22 12:39 fitxers_anna4
-rw-r--r-- 1 a211464sc hisx2	0 feb 22 12:39 fitxers_anna5
-rw-r--r-- 1 a211464sc hisx2	0 feb 22 12:39 fitxers_anna6
-rw-r--r-- 1 a211464sc hisx2	0 feb 22 12:39 fitxers_anna7
-rw-r--r-- 1 a211464sc hisx2	0 feb 22 12:39 fitxers_anna8
-rw-r--r-- 1 a211464sc hisx2	0 feb 22 12:39 fitxers_anna9
-rw-r--r-- 1 a211464sc hisx2   18 feb 22 11:58 README.md
```


----------------------------------------------------------------------------------



