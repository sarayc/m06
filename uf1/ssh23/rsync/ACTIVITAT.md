## Activitat rsync

Tenim dos containers, un ldap i un ssh.
Important instal·lar els paquets:

+ rsync
+ openssh-server
+ openssh-client


### Configuració fitxers

+ vim /etc/rsyncd.conf

```
[man]
	path = /usr/share/man
	comment = manpages del sistema
[doc]
	path = /usr/share/doc
	comment = documentacio del sistema
[practiques_unix01]
	path = /var/tmp/practiques/
	comment = permisos d'escriptura i de lectura
	read only = no
	auth users = unix03, unix01, remot
	secrets file = /etc/rsyncd.secrets

[practiques_unix02]
	path = /var/tmp/practiques/
	comment = permisos nomes de lectura
	read only = yes
	auth users = unix02
	secrets file = /etc/rsyncd.secrets
```

+ vim /etc/rsyncd.secrets --> permisos 600 al fitxer

```
unix01:unix01
unix02:unix02
unix03:unix03
remot:remot
```


### Estructura del directori /var/tmp/practiques

```
remot@ldap:~$ ls -la /var/tmp/
total 16
drwxrwxrwt 1 remot  remot   4096 Feb 25 16:11 .
drwxr-xr-x 1 remot  remot   4096 Nov 20 00:00 ..
drwxrwxrwx 3 nobody nogroup 4096 Feb 25 16:45 practiques
```


### Usuari remot

```
root@ldap:/opt/docker# useradd -m -d /home/remot -s /bin/bash remot
root@ldap:/opt/docker# passwd remot
New password:
Retype new password:
passwd: password updated successfully
```


### Activitats
#### Exercici 1
Al home de l'usuari unix01 hem creat dos fitxers amb touch.

Ordre: rsync -avz /home/unix01 172.24.0.2::practiques_unix01

```
unix01@ssh:~$ rsync -avz /home/unix01 172.24.0.2::practiques_unix01
Password:
sending incremental file list
unix01/
unix01/.bash_history
unix01/.bash_logout
unix01/.bashrc
unix01/.profile
unix01/test1
unix01/test2
unix01/.ssh/
unix01/.ssh/known_hosts
unix01/.ssh/known_hosts.old

sent 3,583 bytes  received 184 bytes  1,506.80 bytes/sec
total size is 5,756  speedup is 1.53
```


Comprovem, anem al container ssh, que es on tenim la configuració i comprovem que tenim tot el home de l’usuari unix01 al directori practiques.

```
remot@ldap:~$ ls -l /var/tmp/practiques/
total 4
drwxr-xr-x 3 nobody nogroup 4096 Feb 25 16:28 unix01

remot@ldap:~$ ls -la /var/tmp/practiques/unix01/
total 28
drwxr-xr-x 3 nobody nogroup 4096 Feb 25 16:28 .
drwxr-xr-x 3 nobody nogroup 4096 Feb 25 16:31 ..
-rw------- 1 nobody nogroup   83 Feb 25 15:23 .bash_history
-rw-r--r-- 1 nobody nogroup  220 Apr 23  2023 .bash_logout
-rw-r--r-- 1 nobody nogroup 3526 Apr 23  2023 .bashrc
-rw-r--r-- 1 nobody nogroup  807 Apr 23  2023 .profile
drwx------ 2 nobody nogroup 4096 Feb 25 15:25 .ssh
-rw-r--r-- 1 nobody nogroup	0 Feb 25 16:28 test1
-rw-r--r-- 1 nobody nogroup	0 Feb 25 16:28 test2
```




#### Exercici 2
Llistem el recurs practiques del host remot.

Ordre: unix01@ssh:~$ rsync 172.24.0.2::practiques_unix01/unix01/

```
unix01@ssh:~$ rsync 172.24.0.2::practiques_unix01
Password:
drwxrwxrwx      	4,096 2024/02/25 16:41:20 .
drwxr-xr-x      	4,096 2024/02/25 16:28:51 unix01

unix01@ssh:~$ rsync 172.24.0.2::practiques_unix01/unix01/
Password:
drwxr-xr-x      	4,096 2024/02/25 16:28:51 .
-rw-------         	83 2024/02/25 15:23:19 .bash_history
-rw-r--r--        	220 2023/04/23 21:23:06 .bash_logout
-rw-r--r--      	3,526 2023/04/23 21:23:06 .bashrc
-rw-r--r--        	807 2023/04/23 21:23:06 .profile
-rw-r--r--          	0 2024/02/25 16:28:12 test1
-rw-r--r--          	0 2024/02/25 16:28:14 test2
drwx------      	4,096 2024/02/25 15:25:51 .ssh
```


```
unix01@ssh:~$ rsync remot@172.24.0.2:/var/tmp/practiques/unix01/
remot@172.24.0.2's password:
drwxr-xr-x      	4,096 2024/02/25 16:28:51 .
-rw-------         	83 2024/02/25 15:23:19 .bash_history
-rw-r--r--        	220 2023/04/23 21:23:06 .bash_logout
-rw-r--r--      	3,526 2023/04/23 21:23:06 .bashrc
-rw-r--r--        	807 2023/04/23 21:23:06 .profile
-rw-r--r--          	0 2024/02/25 16:28:12 test1
-rw-r--r--          	0 2024/02/25 16:28:14 test2
drwx------      	4,096 2024/02/25 15:25:51 .ssh
```




#### Exercici 3
L’usuari unix02 no pot copiar el seu home al recurs de practiques del host remot, perquè només té permisos de lectura.

```
unix02@ssh:~$ rsync -avz /home/unix02 172.24.0.2::practiques_unix02
Password:
sending incremental file list
ERROR: module is read only
rsync error: syntax or usage error (code 1) at main.c(1150) [Receiver=3.2.7]
rsync: [sender] read error: Connection reset by peer (104)
```




#### Exercici 4
Descarreguem el recurs practiques del host remote tot el contingut .ssh.

Ordre: unix02@ssh:/tmp$ rsync -avz 172.24.0.2::practiques_unix02/unix01/.ssh .

```
unix02@ssh:/tmp$ rsync -avz 172.24.0.2::practiques_unix02/unix01/.ssh .
Password:
receiving incremental file list
.ssh/
.ssh/known_hosts
.ssh/known_hosts.old

sent 66 bytes  received 982 bytes  419.20 bytes/sec
total size is 1,120  speedup is 1.07

unix02@ssh:/tmp$ ls -la
total 12
drwxrwxrwt 1 root   root   4096 Feb 25 16:54 .
drwxr-xr-x 1 root   root   4096 Feb 25 15:11 ..
drwx------ 2 unix02 unix02 4096 Feb 25 15:25 .ssh
```
