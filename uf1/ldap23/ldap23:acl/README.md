## LDAP config
### Fitxers necessaris:
+ Dockerfile
+ startup.sh
+ edt-org.ldif
+ slapd.conf


#### Fitxer Dockerfile
```
# Ldapserver 2023
FROM debian:latest
LABEL version="1.0"
LABEL author="@sarayc AISX-M06"
LABEL subject="ldalserver 2023"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y procps iproute2 tree nmap vim ldap-utils slapd less
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 389
```


#### Fitxer startup.sh
```
#! /bin/bash
echo "Configurant el servidor ldap..."


# Esborrar els directoris de configuració i de dades
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*

# Generar el directori de configuració dinàmica slapd.d a partir del fitxer de configuració slapd.conf
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d

# Injectar a baix nivell les dades de la BD 'populate' de l'organització dc=edt,dc=org
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif

# Assignar la propietat i grup del directori de ddaes i de configuració a l'usuari openldap
chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap

# Engegar el servei slapd amb el paràmetre que fa debug per mantenir-lo engegat en foreground
/usr/sbin/slapd -d0
```


### Ordres
**Generem la imatge**
```
docker build -t sarayj03/ldap23:base .
```

**Engegem el container per provar les coses de manera interactiva**
```
docker run --rm -h ldap.edt.org -it sarayj03/ldap23:base /bin/bash
```

**Engegem el container**
```
docker run --rm --name ldap -h ldap.edt.org -d sarayj03/ldap23:base 
```

**Entrem dins del container**
```
docker exec -it ldap /bin/bash
```


### Ordres ldap
Ordres que podem executar:
```
ldapsearch -x -D 'cn=Manager,dc=edt,dc=org' -b 'dc=edt,dc=org'
```

Fora del container:
```
ldapsearch -x -LLL -H ldap://172.17.0.2 -D 'cn=Manager,dc=edt,dc=org' -b 'dc=edt,dc=org'
```

