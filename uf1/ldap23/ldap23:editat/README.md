## LDAP edit
### Fitxers necessaris:
+ Dockerfile
+ startup.sh
+ edt-org.ldif
+ slapd.conf

Editem el fitxer edt-org.ldif


### Ordres
**Generem la imatge**
```
docker build -t sarayj03/ldap23:editat .
```

**Engegem el container**
```
docker run --rm --name ldap -h ldap.edt.org --net 2hisx -p 389:389 -d sarayj03/ldap23:editat 
```

**Entrem al container**
```
docker exec -it ldap /bin/bash
```


### Fitxer slapd.conf
**Generem el password**
```
root@ldap:/opt/docker# slappasswd   
New password: 
Re-enter new password: 
{SSHA}ePdCrf0Xe01ozfNsfl21Vie6253HieCC
```

Ho posem al fitxer slapd.conf
```
database mdb
suffix "dc=edt,dc=org"
rootdn "cn=Manager,dc=edt,dc=org"
# jupiter
rootpw {SSHA}ePdCrf0Xe01ozfNsfl21Vie6253HieCC 
```


**Afegim la base de dades config**
```
database config
rootdn "cn=Sysadmin,cn=config"
rootpw syskey
```


+ Generem la imatge
+ Engegem el container amb propagació de ports
+ Entrem al container



### Comprovacions
#### Comprovació contrasenya 
```
root@ldap:/opt/docker# ldapwhoami -x -D 'cn=Manager,dc=edt,dc=org' -W
Enter LDAP Password: 
dn:cn=Manager,dc=edt,dc=org
root@ldap:/opt/docker# ldapwhoami -x -D 'cn=Manager,dc=edt,dc=org' -w jupiter
dn:cn=Manager,dc=edt,dc=org
```


#### Comprovació del dns dels usuaris
```
ldapsearch -x -LLL -b 'dc=edt,dc=org' uid=Jordi
```
```
dn: uid=Jordi,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Jordi Mas
cn: Giorgios Mas
sn: Mas
homephone: 555-222-2224
mail: jordi@edt.org
description: Watch out for this girl
ou: Alumnes
ou: Profes
uid: jordi
uidNumber: 5004
gidNumber: 100
homeDirectory: /tmp/home/jordi
userPassword: {SSHA}T5jrMgpJwZZgu0azkLIVoYhiG08/KGUv
```


#### Comprovació: modificar la base de dades "edt.org" en calent
**Dins del container**
```
root@e3f1c1977a92:/opt/docker# ldapsearch -x -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess
dn: olcDatabase={1}mdb,cn=config
olcAccess: {0}to *  by self write  by * read
```
```
ldapsearch -x -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess
```

**creem un fitxer**
vim entry.ldif
```
dn: olcDatabase={1}mdb,cn=config
changetype: modify
replace: olcAccess
olcAccess: to * by * read
```

```
ldapmodify -x -D 'cn=Sysadmin,cn=config' -w syskey -f entry.ldif
```

```
root@e3f1c1977a92:/opt/docker# ldapsearch -x -LLL -D 'cn=Sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess
dn: olcDatabase={1}mdb,cn=config
olcAccess: {0}to * by * read
```

