### LDAP 23 practica-schema

+ Pujar-la al git

+ Pujar-la al dockerhub

+ Generar els README.md apropiats

+ Crear un schema amb:

	+ Un nou objecte STRUCTURAL

	+ Un nou objecte AUXILIARU

	+ Cada objecte ha de tenir almenys 3 nous atributs.

	+ Heu d’utilitzar almenys els atributes de tipus boolean, foto (imatge jpeg) i binary per contenir documents pdf.

+ Crear una nova ou anomenada practica.

+ Crear almenys 3 entitats noves dins de ou=practica que siguin dels objectClass definits en l’schema. 

	+ Assegurar-se de omplir amb dades reals la foto i el pdf.

+ Eliminar del slapd.conf tots els schema que no facin falta, deixar només els imprescindibles

+ Visualitzeu amb phpldapadmin les dades, observeu l’schema i verifiqueu la foto i el pdf.


### Engegar el container
Tenim un docker compose que ens engega un phpldapadmin i ldap23:practica
Les imatges són:
+ sarayj03/phpldapadmin
+ sarayj03/ldap23:practica

#### Per engegar
```
docker compose up -d
```

#### Per apagar
```
docker compose down
```


### Creació dels schemas
#### Creació schema concessionari (structural)
```
# concessionari.schema
#
# x-marca
# x-nom
# x-any-fabricacio
# x-estat (venut o no)
# x-foto
# x-cotxes
#
# Derivat de TOP, structural
# ------------------------------------
attributetype (1.1.2.1.1 NAME 'x-marca'
	DESC 'nom de la marca'
	EQUALITY caseIgnoreMatch
	SUBSTR caseIgnoreSubstringsMatch
	SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
	SINGLE-VALUE )

attributetype (1.1.2.1.2 NAME 'x-nom'
	DESC 'nom del cotxe'
	EQUALITY caseIgnoreMatch
	SUBSTR caseIgnoreSubstringsMatch
	SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
	SINGLE-VALUE )

attributetype (1.1.2.1.3 NAME 'x-any-fabricacio'
	DESC 'any del cotxe'
	SYNTAX 1.3.6.1.4.1.1466.115.121.1.27 )

attributetype (1.1.2.1.4 NAME 'x-estat'
	DESC 'venut o no TRUE o FALSE' 
	SYNTAX 1.3.6.1.4.1.1466.115.121.1.7 )

attributetype (1.1.2.1.5 NAME 'x-foto'
	DESC 'foto del cotxe'
	SYNTAX 1.3.6.1.4.1.1466.115.121.1.28 )

attributetype (1.1.2.1.6 NAME 'x-cotxes'
	DESC 'llista de cotxes'
	SYNTAX 1.3.6.1.4.1.1466.115.121.1.5 )

# ----------------------------------------------

objectClass (1.1.2.2.1 NAME 'x-concessionari'
	DESC 'concessionari de cotxes'
	SUP TOP
	STRUCTURAL
	MUST ( x-marca $ x-nom )
	MAY ( x-any-fabricacio $ x-estat $ x-foto $ x-cotxes ))
```


#### Creació schema deportius (auxiliar)
```
# deportius.schema

# x-color
# x-potencia
# x-numero

# Derivat de x-concessionari, auxiliary
# ------------------------------------------------------
attributetype (1.1.2.2.1 NAME 'x-numero'
    DESC 'numero de cotxes deportius que té el concessionari'
    EQUALITY integerMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.27
    SINGLE-VALUE )

attributetype (1.1.2.2.2 NAME 'x-color'
    DESC 'color del cotxe deportiu'
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
    SINGLE-VALUE )

attributetype (1.1.2.2.3 NAME 'x-potencia'
    DESC 'potencia que té el cotxe deportiu'
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.27
    SINGLE-VALUE )

objectclass (1.1.2.2.2 NAME 'x-deportius'
	DESC 'cotxes deportius al concessionari'
	SUP TOP
	AUXILIARY
	MUST ( x-numero )
	MAY ( x-color $ x-potencia ))
```

### Creació d'una nova ou anomenada pràctica
```
dn: ou=practica,dc=edt,dc=org
ou: practica
description: container de practica
objectClass: organizationalunit
```

### Creació de cinc entitats dins de ou=practica
```
dn: x-nom=Honda Civic,ou=practica,dc=edt,dc=org
objectClass: x-concessionari
x-nom: Honda Civic
x-foto:<file:///opt/docker/honda_civic.jpeg
x-cotxes:<file:///opt/docker/honda_civic.pdf
x-marca: Honda
x-any-fabricacio: 2010
x-estat: FALSE
```
```
dn: x-nom=Lamborghini huracan,ou=practica,dc=edt,dc=org
objectClass: x-concessionari
objectClass: x-deportius
x-nom: Lamborghini huracan
x-foto:<file:///opt/docker/lamborghini_huracan.jpg
x-cotxes:<file:///opt/docker/lamborghini_huracan.pdf
x-marca: Lamborghini
x-any-fabricacio: 2012
x-estat: FALSE
x-numero: 4
x-color: verd
```
```
dn: x-nom=Seat Leon,ou=practica,dc=edt,dc=org
objectClass: x-concessionari
x-nom: Seat Leon
x-foto:<file:///opt/docker/seat_leon.jpg
x-cotxes:<file:///opt/docker/seat_leon.pdf
x-marca: Seat
x-any-fabricacio: 2014
x-estat: TRUE
```
```
dn: x-nom=Bugatti Chiron,ou=practica,dc=edt,dc=org
objectClass: x-concessionari
objectClass: x-deportius
x-nom: Bugatti Chiron
x-foto:<file:///opt/docker/bugatti_chiron.jpg
x-cotxes:<file:///opt/docker/bugatti_chiron.pdf
x-marca: Bugatti
x-any-fabricacio: 2020
x-numero: 2
x-estat: FALSE
x-color: negra
x-potencia: 1500CV
```
```
dn: x-nom=Seat Ibiza,ou=practica,dc=edt,dc=org
objectClass: x-concessionari
x-nom: Seat Ibiza
x-foto:<file:///opt/docker/seat_ibiza.jpg
x-cotxes:<file:///opt/docker/seat_ibiza.pdf
x-marca: Seat
x-any-fabricacio: 2015
x-estat: FALSE
```


### Revisem el fitxer slapd.conf
Revisem que esborrem tots els schemas que no ens facin falta.


### Visualitzem amb phpldapadmin
Anem al navegador i comprovem 
```
localhost/phpldapadmin
```
