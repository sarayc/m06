## LDAP edit
### Fitxers necessaris:
+ Dockerfile
+ startup.sh
+ edt-org.ldif
+ slapd.conf
+ futbolista.schema

### Casos a implementar:
+ estructural derivat de inetOrgPerson
+ Estructural derivat de TOP
+ auxiliary derivat de TOP

#### Estructural derivat de inetOrgPerson

Fitxer futbolista.schema
```
# futbolista.schema
# 
# x-nom
# x-equip
# x-dorsal
# x-web
# x-foto
# x-lesionat
#
# Derivat de TOP, Auxiliary
# -----------------------------------------------

attributetype (1.1.2.1.1.1 NAME 'x-equip'
  DESC 'nom del equip'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE )

attributetype (1.1.2.1.1.2 NAME 'x-dorsal'
  DESC 'dorsal del jugador'
  EQUALITY integerMatch
  ORDERING integerOrderingMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.27
  SINGLE-VALUE )

attributetype (1.1.2.1.1.3 NAME 'x-web'
  DESC 'pagina(s) web del jugador'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 )

attributetype (1.1.2.1.1.4 NAME 'x-foto'
  DESC 'foto(s) del jugador'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.28 )

attributetype (1.1.2.1.1.5 NAME 'x-lesionat'
  DESC 'lesionat true/false'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.7
  SINGLE-VALUE )

# -----------------------------------------------

objectClass (1.1.2.2.1.1 NAME 'x-futbolista'
  DESC 'Futboleros crazys'
  SUP inetOrgPerson
  STRUCTURAL
  MUST x-equip
  MAY ( x-dorsal $ x-web $ x-foto $ x-lesionat )
  )
```

Fitxer data-futbolA.ldif
```
dn: cn=dembele,ou=usuaris,dc=edt,dc=org
objectClass: x-futbolista
cn: dembele
sn: lo no crack
x-equip: psg
x-lesionat: TRUE
```

Engegem el container
```
docker run --rm --name ldap -h ldap.edt.org --net 2hisx -d sarayj03/ldap23:schema
```

Ens conectem amb exec
```
docker exec -it ldap /bin/bash
```

Realitzem la següent ordre:
```
ldapadd -x -D 'cn=Manager,dc=edt,dc=org' -w secret -f data-futbolA.ldif
```

Comprovem fent per exemple
```
slapcat
```


--------------------------------------------------------------------------

#### Estructural standalone object
Fitxer futbolista.schema
```
# futbolista.schema
# 
# x-nom
# x-equip
# x-dorsal
# x-web
# x-foto
# x-lesionat
#
# Derivat de TOP, Auxiliary
# -----------------------------------------------

attributetype (1.1.2.1.1.1 NAME 'x-equip'
  DESC 'nom del equip'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE )

attributetype (1.1.2.1.1.2 NAME 'x-dorsal'
  DESC 'dorsal del jugador'
  EQUALITY integerMatch
  ORDERING integerOrderingMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.27
  SINGLE-VALUE )

attributetype (1.1.2.1.1.3 NAME 'x-web'
  DESC 'pagina(s) web del jugador'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 )

attributetype (1.1.2.1.1.4 NAME 'x-foto'
  DESC 'foto(s) del jugador'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.28 )

attributetype (1.1.2.1.1.5 NAME 'x-lesionat'
  DESC 'lesionat true/false'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.7
  SINGLE-VALUE )

attributetype (1.1.2.1.1.6 NAME 'x-nom'
  DESC 'nom del futbolista'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE )

# -----------------------------------------------

objectClass (1.1.2.2.1.1 NAME 'x-futbolista'
  DESC 'Futboleros crazys'
  SUP TOP
  MUST ( x-equip $ x-nom )
  MAY ( x-dorsal $ x-web $ x-foto $ x-lesionat )
  )
```

Fitxer data-futbolB.ldif
```
dn: x-nom=dembele,ou=usuaris,dc=edt,dc=org
objectClass: x-futbolista
x-equip: barca
x-lesionat: TRUE
```

```
ldapadd -x -D 'cn=Manager,dc=edt,dc=org' -w secret -f data-futbolB.ldif 
```

-------------------------------------------------------

#### top auxiliary
Fitxer futbolista.schema
```
# futbolista.schema
# 
# x-nom
# x-equip
# x-dorsal
# x-web
# x-foto
# x-lesionat
#
# Derivat de TOP, Auxiliary
# -----------------------------------------------

attributetype (1.1.2.1.1.1 NAME 'x-equip'
  DESC 'nom del equip'
  EQUALITY caseIgnoreMatch
  SUBSTR caseIgnoreSubstringsMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
  SINGLE-VALUE )

attributetype (1.1.2.1.1.2 NAME 'x-dorsal'
  DESC 'dorsal del jugador'
  EQUALITY integerMatch
  ORDERING integerOrderingMatch
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.27
  SINGLE-VALUE )

attributetype (1.1.2.1.1.3 NAME 'x-web'
  DESC 'pagina(s) web del jugador'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 )

attributetype (1.1.2.1.1.4 NAME 'x-foto'
  DESC 'foto(s) del jugador'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.28 )

attributetype (1.1.2.1.1.5 NAME 'x-lesionat'
  DESC 'lesionat true/false'
  SYNTAX 1.3.6.1.4.1.1466.115.121.1.7
  SINGLE-VALUE )

# -----------------------------------------------

objectClass (1.1.2.2.1.1 NAME 'x-futbolista'
  DESC 'Futboleros crazys'
  SUP TOP
  AUXILIARY
  MUST x-equip
  MAY ( x-nom $ x-dorsal $ x-web $ x-foto $ x-lesionat )
  )
```

Fitxer data-futbolC.ldif
```
dn: cn=ramon ferrer,ou=usuaris,dc=edt,dc=org
objectClass: inetOrgPerson
objectClass: x-futbolista
cn: ramon ferrer
sn: ramon
homePhone: 123456789
mail: ramon@edt.org
x-nom: dembele
x-equip: solters
x-lesionat: FALSE

dn: cn=ferran roig,ou=usuaris,dc=edt,dc=org
objectClass: inetOrgPerson
objectClass: x-futbolista
cn: ferran roig
sn: ferran
x-equip: espanyol
x-dorsal: 4
mail: ferran@edt.org
```
